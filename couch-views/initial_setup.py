from uuid import uuid4
import couchdb


dbs = ['lookup', 'case', 'form', 'usage']

users = {
    'pythonuser':'${python_couchdb_password}',
    'public':'public',
    'admin':'${couchdb_admin_password}',
    'private':str(uuid4())
}

user_doc = '''{"_id": "org.couchdb.user:%s","name": "%s","roles": [],"type": "user","password": "%s"}'''

secure_doc = '''{"admins":{"names":["admin"],"roles":[]},"members":{"names":["private"],"roles":[]}}'''
open_doc = '''{"admins":{"names":[],"roles":[]},"members":{"names":["public"],"roles":[]}}'''

#_couch_server_string =  "http://%s:%s@%s" % (_couch_user, _couch_pw, _couch_url)

server = couchdb.Server('http://localhost:5984')

db = server['_users']

user_base = 'org.couchdb.user:%s'
user_name = 'pythonuser'
first_user = user_base % (user_name)
db[first_user] = eval(user_doc % (first_user, user_name, users[user_name]))




