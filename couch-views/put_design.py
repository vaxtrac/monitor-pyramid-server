import sys
import os
import json
import couchdb


_couch_user = "pythonuser"
_couch_pw = "${python_couchdb_password}"
_couch_url = "localhost:5984"
_couch_server_string =  "http://%s:%s@%s" % (_couch_user, _couch_pw, _couch_url)


suffixes = ["case", "form", "usage", "lookup", "_users"]

server = couchdb.Server(_couch_server_string)


def put_docs(db_name, base_path):
    try:
        db = server[db_name]
    except Exception ,e:
        print "%s! Creating %s" % (e, server.create(db_name))
        db = server[db_name]
    view_files = os.listdir(base_path)
    for vf in view_files:
        put_doc(db, base_path, vf)


def put_doc(db, base_path, vf):
    f_path = "%s/%s" % (base_path, vf)
    with open(f_path , "r") as f:
        if vf[0] != "_" and vf[0:3] != "org":
            v_name = "_design/%s" % (vf.split(".json")[0])
        else:
            v_name = "%s" % (vf.split(".json")[0])
        contents = f.read()
        print "new view %s" % (v_name)
        try:
            db[v_name] = contents
        except Exception, e:
            if vf[0] == "_":
                raise Exception("ignoring view_file with leading _: %s" % (e))
            elif vf[0:3] == "org":
                raise Exception("ignoring view_file containsing org (is a security file) : %s" % (e))
            doc = db[v_name]
            print "View %s exists! _rev: %s" % (v_name, doc.get("_rev"))
            new_doc = json.loads(contents)
            nd_rev = new_doc.get("_rev")
            print "rev in doc to upload: %s" % (nd_rev)
            new_doc["_rev"] = doc.get("_rev")
            res = db.save(new_doc)
            print "updating... | %s" % (res,)


def manage_update(args):
    if len(args) < 3:
        print "no view provided for update"
        return
    _file = args[2]
    if not ".json" in _file:
        print "update file should be json"
        return
    if not os.path.exists(_file):
        print "couldn't find file %s to update" % (_file)
        return
    try:
        base_path, view_file = os.path.split(_file)
        db_match = [s for s in suffixes if s in base_path]
        if len(db_match) < 1:
            raise ValueError("no matching database found in path: %s... " % (base_path))
        print "matching db: %s | path %s | file %s" % (db_match[0], base_path, view_file,)
        db = None
        try:
            db = server[db_match[0]]
        except Exception as e2:
            print "couldn't connect to couchdb server: %s" % (e2.__class__)
            return
        put_doc(db, base_path, view_file)
    except Exception as e:
        print "couldn't complete view update err: %s" % (e)




def handle_commands(args):
    if "update_view" in str(args[1]):
        manage_update(args)
    else:
        print "unknown command: %s" % (args[1])

def put(from_base):
    if not os.path.exists("./%s" % from_base):
        print "no source directory"
    for suffix in suffixes:
        db_name = suffix
        print "~*~*~*~*~  next DB to populate | %s ~*~*~*~*~" % ( db_name )
        base_path = "./%s/%s" % (from_base, db_name)
        if not os.path.exists(base_path):
            print "missing directory %s" % (base_path)
        put_docs(db_name, base_path)

if __name__ == "__main__":
    argv = sys.argv
    if len(argv)  > 1:
        handle_commands(argv)
    else:
        print "installing all views from clean folder"
        put("clean")
