import subprocess, os, sys, shutil

#takes _dev components, processes and copies them over to live version
# REQUIRES from Node and the following packages from NPM ng-annotate, uglify-js

HERE = os.path.dirname(os.path.realpath(__file__))
BASE = "%s/monitor" % HERE
STATIC_DEV = "%s/static_dev" % BASE
STATIC = "%s/static" % BASE
TEMPS_DEV = '%s/templates_dev' % BASE
TEMPS = '%s/templates' % BASE


def minifyJS(src, dst):
    #dst = dst.replace('.js', '.min.js')
    cmd = 'ng-annotate -a %s | uglifyjs > %s' % ( src, dst )
    op = subprocess.check_call(cmd , shell=True)
    pass

def processAppFolder(arg, src_base, names):
    dst_base = src_base.replace(*arg)
    print "%s" % ([dst_base, src_base, names],)
    for name, ext in [os.path.splitext(i) for i in names]:
        if ext:
            filename = "%s%s" % (name, ext)
            src = "%s/%s" % (src_base, filename)
            dst = "%s/%s" % (dst_base, filename)
            if ext == ".js":
                print "minify %s" % (src)
                try:
                    minifyJS(src, dst)
                except Exception , e:
                    print e
                    sys.exit("COPY FAILED!\nnode uglifyjs and ng-annotate are required for this script\nplease install: 'sudo npm install -g uglify-js ng-annotate'")
            else:
                print 'copy %s' % (src)
                shutil.copy(src, dst)
                #just copy
        else:
            #just make the directory
            dst_folder = "%s/%s" % (dst_base, name)
            if not os.path.exists(dst_folder):
                print "make folder %s" % ([dst_folder],)
                os.mkdir(dst_folder)

def main():
    print "%s" % ([HERE,BASE,STATIC_DEV, STATIC, TEMPS_DEV, TEMPS],)
    #remove old templates
    try:
        shutil.rmtree(TEMPS)
    except Exception ,e:
        print e
        print 'moving on...'
    #migrate templates
    shutil.copytree(TEMPS_DEV, TEMPS)
    #remove old static assets
    try:
        shutil.rmtree(STATIC)
    except Exception ,e:
        print e
        print 'moving on...'
    os.mkdir(STATIC)
    #migrate folders NOT named app
    for obj in os.listdir(STATIC_DEV):
        src = '%s/%s' % (STATIC_DEV, obj)
        dst = '%s/%s' % (STATIC, obj)
        if obj != 'app':
            if os.path.isdir(src):
                #copy tree
                print 'copying DIR recursively %s | --> | %s' % (src, dst)
                shutil.copytree(src,dst)
            else:
                print 'copying FILE %s | --> | %s' % (src, dst)
                shutil.copy(src, dst)
        else:
            try:
                os.mkdir(dst)
                os.path.walk(src, processAppFolder, [STATIC_DEV, STATIC])
            except Exception,e:
                print e

            print 'migrating angular app'




if __name__ == "__main__":
    main()
