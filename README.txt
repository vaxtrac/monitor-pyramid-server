This project has many dependencies and is best setup on a fresh Ubuntu Server 
via the installer.

Once setup, you can serve a development version to a different port or maintain 
a dev subdomain via the is_production flag in the development.ini.

Changes are pushed from the structure to live through go-live.py in the base folder.