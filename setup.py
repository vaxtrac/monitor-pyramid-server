import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.txt')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()

requires = [
    'couchdb',
    'velruse',
    'passlib',
    'pyramid',
    'pyramid_chameleon',
    'pyramid_debugtoolbar',
    'pyramid_tm',
    'pyramid_beaker',
    'Babel',
    'SQLAlchemy',
    'transaction',
    'zope.sqlalchemy',
    'waitress',
    ]

extractors = {'monitor': [
            ('**.py', 'python', None),
            ('templates/**.html', 'mako', None),
            ('templates/**.mako', 'mako', None),
            ('templates_dev/**.html', 'mako', None),
            ('templates_dev/**.mako', 'mako', None),
            ('static/**', 'ignore', None),
            ('static_dev/**', 'ignore', None),]}



setup(name='monitor',
      version='0.0',
      description='monitor',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='',
      author_email='',
      url='',
      keywords='web wsgi bfg pylons pyramid',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='monitor',
      install_requires=requires,
      message_extractors=extractors,
      entry_points="""\
      [paste.app_factory]
      main = monitor:main
      [console_scripts]
      initialize_monitor_db = monitor.scripts.initializedb:main
      """,
      )
