import json
import os
from pyramid.path import AssetResolver
from monitor import STATIC_ROUTES


class Helper(object):

    package = 'monitor'
    route = "help_res"

    def __init__(self):
        self.resources = {}
        try:
            a = AssetResolver(Helper.package)
            resolver = a.resolve(Helper.route)
            path = resolver.abspath()
            route_end = STATIC_ROUTES.get(Helper.route)
            path = path.split(Helper.route)[0] + route_end
            folders = [obj for obj in os.listdir(path) if os.path.isdir(os.path.join(path, obj))]
            for folder in folders:
                np = os.path.join(path, folder)
                with open(os.path.join(np, "help.json"), "r") as f:
                    self.resources[folder] = json.load(f)

            self.keys = [res.replace(".", "/") for res in self.resources.keys()]
        except Exception, e:
            print e
            pass

    def match_resource(self, url):
        print "matching url %s" % url
        distances = [(key, self.levenshteinDistance(url, key)) for key in self.keys]
        best_key = min(distances, key = lambda t: t[1])
        url_match = best_key[0].replace("/", ".")
        return {"url": url_match , "resources": self.resources[url_match]}


    def levenshteinDistance(self, s1, s2):
        if len(s1) > len(s2):
            s1, s2 = s2, s1

        distances = range(len(s1) + 1)
        for i2, c2 in enumerate(s2):
            distances_ = [i2+1]
            for i1, c1 in enumerate(s1):
                if c1 == c2:
                    distances_.append(distances[i1])
                else:
                    distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
            distances = distances_
        return distances[-1]


HELPER = Helper()

