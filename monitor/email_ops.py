
import smtplib

import email

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from monitor.settings import Settings
SETTINGS = Settings

EMAIL_PW = SETTINGS['stmp_password']
USERNAME = SETTINGS['stmp_user']
STMP_URL = SETTINGS['stmp_url']
STMP_PORT = int(SETTINGS['stmp_port'])
SEVER_URL = SETTINGS['server_url']

def send_email_reset(recipient, token):
    gmail_user = USERNAME
    gmail_pwd = EMAIL_PW
    FROM = USERNAME
    server_url = SEVER_URL
    TO = recipient
    SUBJECT = "VaxTrac Monitor: Password Reset"
    LINK = '%s/admin/setpassword?email=%s&token=%s' % (server_url, recipient, token)
    TEXT = 'Hello %s,\nYour VaxTrac Monitor password needs to be reset.\nPlease paste the following url into your browser:\n%s' % (recipient, LINK)

    HTML = '''\
        <html>
          <head></head>
          <body>
            <p>Hello %s<br>
               Your VaxTrac Monitor password needs to be reset.<br>
               Please follow <a href="%s">this link </a> to complete the process.<br>
            </p>
          </body>
        </html>
    ''' % (recipient, LINK)

    # Prepare actual message
    msg = MIMEMultipart('alternative')
    msg['Subject'] = SUBJECT
    msg['From'] = FROM
    msg['To'] = TO

    part1 = MIMEText(TEXT, 'plain')
    part2 = MIMEText(HTML, 'html')

    msg.attach(part1)
    msg.attach(part2)

    try:
        server = smtplib.SMTP(STMP_URL, STMP_PORT)
        server.ehlo()
        server.starttls()
        server.login(gmail_user, gmail_pwd)
        server.sendmail(FROM, TO, msg.as_string())
        server.close()
        print 'successfully sent the mail'
    except Exception, e:
        print e
        print "failed to send mail"

