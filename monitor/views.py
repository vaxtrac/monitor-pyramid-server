from pyramid.httpexceptions import (
    HTTPFound,
    HTTPNotFound,
    HTTPForbidden,
    HTTPBadRequest
    )
from pyramid.response import Response
from pyramid.security import (
    remember,
    forget,)
from pyramid.view import view_config
from velruse import login_url

from couchdbapi import CouchAPI, WCouch
from sqlalchemy.exc import DBAPIError

from .models import (
    DBSession,
    Users,
    Translations,
    Base,
    AuthTokens,
    Passwords,
    validatePassword
    )

from help import HELPER
from email_ops import send_email_reset
from settings import Settings as SETTINGS

import json
import data

'''

    Error Routes

'''
@view_config(
    context=HTTPForbidden,
    renderer="403.mako"
    )
def view_forbidden(request):
    request.response.status = 403
    return {"login_url": request.route_url('login')}


@view_config(
    context=HTTPNotFound,
    renderer="404.mako"
    )
def view_missing(request):
    request.response.status = 404
    return {}

'''

    Login/Logout Routes

'''

@view_config(
    route_name='login',
    permission='unsecured',
    renderer="login.mako",
    request_method="GET"

    )
def view_login_get(request):
    return {"project": "monitor.vaxtrac.com",
            "login_url": login_url(request,'google'),
            "login_failed": False,
            "this_url": request.route_url('login')
           }

@view_config(
    route_name='login',
    permission='unsecured',
    renderer="login.mako",
    request_method="POST"
    )
def view_login_post(request):
    rp = request.POST
    try:
        if rp['login'] and rp['password']:
            email = rp['login']
            pw = rp['password']
            ok = Passwords.checkPassword(email, pw)
            if not ok:
                raise KeyError("Login and password don't match")
            else:
                user = Users.getByAttribute("username", email)
                headers = remember(request, user.username)
                try:
                    return HTTPFound(location = request.route_url('home'),
                         headers = headers)
                except Exception, e:
                    print e
                    return HTTPFound(location = request.route_url('home'),
                         headers = headers)

        else:
            raise KeyError("Login and password not included")
    except Exception,e:
        print "error in login | %s" % (str(e),)
        return {"project": "monitor.vaxtrac.com",
                "login_url": login_url(request,'google'),
                "login_failed": True,
                "this_url": request.route_url('login')
               }


@view_config(
    route_name='logout',
    permission='logged',
    renderer='json')
def logout(request):
    headers = forget(request)
    return HTTPFound(location = request.route_url('home'),
                     headers = headers)

'''

    Login

'''

##GOOGLE Oath Callback

@view_config(
    context='velruse.AuthenticationComplete',
    renderer='403.mako',
)
def login_complete_view(request):
    profile = request.context.profile
    registered = Users.register_oauth(profile)
    if not registered.get('success'):
        #Send to 403 Page w/ explanation
        request.response.status = 403
        return {"failed":registered.get('reason')}
    user = registered.get('user')
    headers = remember(request, user.username)
    #send home
    try:
        return HTTPFound(location=request.route_url('ticker'), headers=headers)
    except Exception, e:
        print e
        return HTTPFound(location=request.route_url('home'), headers=headers)

@view_config(
    context='velruse.AuthenticationDenied',
    renderer='json',
)
def login_denied_view(request):
    return HTTPForbidden()


# reset_password endpoint (accessed from login screen)

@view_config(
    route_name='reset_password',
    permission='unsecured',
    renderer='admin_forgot_password.mako',
    request_method="GET"
    )
def admin_reset_password_get(request):
    print "reset password hit!"

    return {"reset_ok": False,
            "reset_msg":None,
            "this_url": request.route_url('reset_password')
           }

@view_config(
    route_name='reset_password',
    permission='unsecured',
    renderer='admin_forgot_password.mako',
    request_method="POST"
    )
def admin_reset_password_post(request):
    print "reset password hit!"
    rt = request.POST
    pkg = {"reset_ok": False,
            "reset_msg":"Email not found!",
            "this_url": request.route_url('reset_password')
           }
    try:
        email = rt['login']
        user = Users.getByAttribute("username", email)
        if not user:
            raise KeyError('no user with that email')
        token = AuthTokens.createToken(email)
        send_email_reset(email, token.get('value'))
        pkg["reset_ok"] = True
        pkg["reset_msg"] = ''
        return pkg
    except Exception,e:
        return pkg



# set_password (requires token)
@view_config(
    route_name='set_password',
    permission='unsecured',
    renderer='admin_set_password.mako',
    request_method="GET"
    )
def admin_set_password_view(request):
    failed = "unknown"
    rt = request.params
    try:
        if rt['token'] and rt['email']:
            return {
                "form_ok": True,
                "token": rt['token'],
                "login": rt['email'],
                "this_url": request.route_url('set_password')
            }
        else:
            raise ValueError("Missing Token or Login")

    except Exception,e:
            return {
                "form_ok": False,
                "form_msg": "Missing Token or Email",
            }

# post password to be set from form
@view_config(
    route_name='set_password',
    permission='unsecured',
    renderer='admin_set_password.mako',
    request_method="POST"
    )
def admin_set_password_post_json(request):
    print "set password hit!"
    rt = request.POST
    pkg= {
        "form_ok": False,
        "form_msg": "Error setting password."
    }
    try:
        '''
        if rt['pw1'] != rt['pw2']:
            pkg['form_msg'] += "\nPasswords don't match."
            raise ValueError(pkg['form_msg'])
        elif len(rt['pw1']) < 8:
            pkg['form_msg'] += "\nPassword must be 8 characters long."
            raise ValueError(pkg['form_msg'])
        '''
        validatePassword(rt, pkg)
        email = rt['login']
        user = Users.getByAttribute("username", email)
        if not user:
            raise ValueError("user not found : %s" % email)
        token = rt['token']
        if not token:
            raise ValueError("no token...")
        token_ok = AuthTokens.redeemToken(email, token)
        if not token_ok:
            raise ValueError("bad token...")

        Passwords.setPassword(email, rt['pw1'])
        return HTTPFound(location=request.route_url('login'))

    except Exception,e:
        print e
        return pkg

'''

    Views Routes

'''

#AdministrativeViews


#view users to make changes
@view_config(
    route_name='admin_users',
    permission='admin',
    renderer='admin-users.mako',
)
def administer_users_view(request):
    return {}

#list of users and their mutable data
@view_config(
    route_name='admin_users_json',
    permission='admin',
    renderer='json',
    request_method="GET",
)
def administer_users_json_get(request):
    return json.dumps(Users.getUserList())

# post changes to mutable data

@view_config(
    route_name='admin_users_json',
    permission='admin',
    renderer='json',
    request_method="POST",
)
def administer_users_json_post(request):
    data = request.json_body
    cache = json.loads(str(data.get('cache')))
    mutable = ["firstname","lastname","groups"]
    if "groups" in cache.keys():
        valid_groups = ['demo', 'basic', 'admin', 'developer']
        max_permission = valid_groups.index( str(cache.get('groups')) )
        cache['groups'] = json.dumps(valid_groups[:max_permission+1])
    userid = data.get('userid')
    out = {}
    if not userid:
        return HTTPNotFound()
    for attr in mutable:
        val = cache.get(attr)
        if val:
            res = Users.setUserAttribute(userid, attr, val)
            if res:
                print "setting %s to %s %s" % (attr, val, res)
                out[attr] = val
        else:
            print "%s is no present" % (attr)

    return json.dumps({"updated": out})


#ADD USER via admin panel
@view_config(
    route_name='admin_user_add_json',
    permission='admin',
    renderer='json',
    request_method="POST",
)
def administer_user_add_json_post(request):
    data = request.json_body
    cache = json.loads(str(data.get('cache')))
    if "groups" in cache.keys():
        valid_groups = ['demo', 'basic', 'admin', 'developer']
        max_permission = valid_groups.index( str(cache.get('groups')) )
        cache['groups'] = json.dumps(valid_groups[:max_permission+1])
    try:
        validatePassword(cache)
    except ValueError, e:
        print e
        #failed
        return HTTPNotFound()
    res = Users.register(cache)
    if not res.get('success'):
        print "didn't register user, failing"
        return HTTPNotFound()
        #failed
    else:
        Passwords.setPassword(cache['email'], cache['pw1'])
    return json.dumps({"updated": True})


#set language cookie
@view_config(route_name='lang')
def set_user_lang(request):
    code = request.matchdict['code']
    code = "/en/%s" % code
    print "code: %s" % code
    response = Response()
    response.set_cookie('lang', value=code, max_age=31536000) # max_age = year
    return HTTPFound(location=request.route_url('home'), headers=response.headers)

#usages list (pulls straight from couchdbapi in angular)
@view_config(
    route_name='usage_total',
    permission='admin',
    renderer='usage_total.mako',
)
def total_usage_view(request):
    return {
        'authenticated': 'true',
    }

#usage of a single user (js app pulls straight from couchdb api)
@view_config(
    route_name='usage_user',
    permission='admin',
    renderer='usage_user.mako',
)
def user_usage_view(request):
    userName = request.matchdict['user_name']
    return {
        "userName": userName
    }

#
#    ClinicViews
#

@view_config(
    route_name='clinic_status',
    permission='basic',
    renderer='clinic_status.mako')
def clinic_status_view(request):
    clinicName = request.matchdict['clinic_name']
    clinicID = data.getIDforClinic(clinicName)
    return {"clinicName": clinicName, "clinicID": clinicID}


@view_config(
    route_name='clinic_map',
    permission='basic',
    renderer='clinic_map.mako'
    )
def clinic_map_view(request):
    return {"google_maps_api_key":SETTINGS.get('google_maps_api_key')}

@view_config(
    route_name='clinic_browse',
    permission='basic',
    renderer='clinic_browse.mako'
    )
def clinic_browse_view(request):
    return {}

#
#    M&E Views
#

@view_config(
    route_name='network_map',
    permission='logged',
    renderer='map-network.mako'
    )
def network_map_view(request):
    return {"google_maps_api_key":SETTINGS.get('google_maps_api_key')}

@view_config(
    route_name='me_latency',
    permission='logged',
    renderer='me-latency.mako'
    )
def me_latency_view(request):
    return {}

@view_config(
    route_name='me_form_type',
    permission='logged',
    renderer='me-form-type.mako'
    )
def me_form_type_view(request):
    return {}

@view_config(
    route_name='me_adherence',
    permission='logged',
    renderer='me-adherence.mako'
    )
def me_adherence_view(request):
    doses = {
        "OPV":[0,1,2,3],
        "BCG":[0],
        "DTwPHibHep":[0,1,2],
        "IPV":[0],
        "Measles":[0],
        "Pneumo_conj":[0,1,2],
        "VitaminA":[0],
        "YF":[0]
    }
    vaccine = request.matchdict['vaccine']
    return {"vaccine": vaccine, "doses": doses.get(vaccine, [0])}

@view_config(
    route_name='me_adherence_reported',
    permission='logged',
    renderer='me-adherence-reported.mako'
    )
def me_adherence_reported_view(request):
    doses = {
        "OPV":[0,1,2,3],
        "BCG":[0],
        "DTwPHibHep":[0,1,2],
        "IPV":[0],
        "Measles":[0],
        "Pneumo_conj":[0,1,2],
        "VitaminA":[0],
        "YF":[0]
    }
    vaccine = request.matchdict['vaccine']
    return {"vaccine": vaccine, "doses": doses.get(vaccine, [0])}

@view_config(
    route_name='me_adherence_select',
    permission='logged',
    renderer='me-adherence-select.mako'
    )
def me_adherence_view_select(request):
    doses = {
        "OPV":[0,1,2,3],
        "BCG":[0],
        "DTwPHibHep":[0,1,2],
        "IPV":[0],
        "Measles":[0],
        "Pneumo_conj":[0,1,2],
        "VitaminA":[0],
        "YF":[0]
    }
    return {"doses": doses.keys()}

@view_config(
    route_name='me_adherence_select_reported',
    permission='logged',
    renderer='me-adherence-select-reported.mako'
    )
def me_adherence_reported_view_select(request):
    doses = {
        "OPV":[0,1,2,3],
        "BCG":[0],
        "DTwPHibHep":[0,1,2],
        "IPV":[0],
        "Measles":[0],
        "Pneumo_conj":[0,1,2],
        "VitaminA":[0],
        "YF":[0]
    }
    return {"doses": doses.keys()}

#
#    Help View
#

@view_config(
    route_name="help",
    permission="basic",
    renderer="help.mako"
)
def help_view(request):
    params = request.params
    if not params.get('page'):
        request.override_renderer = 'help_menu.mako'
        out = {'names': HELPER.keys}
        return out
    out = {}
    out['page'] = params['page']
    out.update(HELPER.match_resource(out.get('page')))
    return out

#
#   JavaScript Translations API
#

#/api/trans/
@view_config(
    route_name="request_new_trans",
    permission="logged",
    renderer="json"
)
def new_trans_json(request):
    rt = request.json_body
    print "new translations request %s" % (rt)
    lang = rt.get('lang')
    query = rt.get('query')
    print "%s | %s" % (lang,query)
    if None in [lang, query]: return HTTPBadRequest()
    success = Translations.addWord(query, lang)
    return json.dumps({"word_added": success})



@view_config(
    route_name="get_translations",
    permission="logged",
    renderer="json"
)
def get_translations_json(request):
    params = request.params
    lang = params.get('lang')
    print "translation request for lang: %s" % (lang)
    try:
        if not lang:
            return json.dumps({})
        return json.dumps(Translations.registeredWords(lang))
    except Exception,e:
        print e
        #print Translations.__table__.create(DBSession.get_bind()) ## create new DB from model
        return {}



#
#    CouchDB API
#

@view_config(
    route_name='couchdb_api',
    permission='logged',
    renderer='json')
def couchdb_json_api(request):
    rt = request.matchdict
    params = request.params
    if not params.get("filter", False) == "true":
        print "API call without filter"
        return CouchAPI.view(rt['db'], rt['design'], rt['view'], **params)
    else:
        print "API call WITH filter"
        p = {}
        for k in params:
            if k != "filter" and k != "cache":
                try:
                    p[str(k)] = json.loads(str(params.get(k)).replace("'",""))
                except Exception,e:
                    print e
                    p[k] = params.get(k)
        print p
        if params.get('cache') == "true":
            print "getting cached query"
            return WCouch.cachedQuery(rt['db'], rt['design'], rt['view'], **p)
        else:
            return WCouch.filterQuery(rt['db'], rt['design'], rt['view'], **p)

@view_config(
    route_name='couchdb_changes',
    permission='logged',
    renderer='json')
def couchdb_json_changes(request):
    rt = request.matchdict
    params = request.params
    return CouchAPI.changes(rt['db'], **params)


#CSVOutputAPI

@view_config(
    route_name='csv_api',
    permission='logged')
def csv_api(request):
    response = Response()
    response.headers['Content-type'] = 'text/csv'
    response.headers['Content-Disposition'] = 'attachment; filename="report.csv"'
    rt = request.matchdict
    json_content = request.json_body
    csv_content = data.getCSVfromChartJSON(json_content)
    response.app_iter = csv_content
    return response


#Home View

@view_config(
    route_name='home',
    permission='logged',
    renderer='ticker.mako',
)
def ticker_view(request):
    return {"google_maps_api_key":SETTINGS.get('google_maps_api_key')}

