from pyramid.config import Configurator
from pyramid_beaker import session_factory_from_settings, set_cache_regions_from_settings
from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.settings import asbool
from sqlalchemy import engine_from_config

from .settings import init_settings

from .models import (
    DBSession,
    Base,
    RootFactory,
    groupFinder,
    getUser
    )

STATIC_ROUTES = {}

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    init_settings(settings)
    #auto policy
    #TODO remove these secrets!
    cookie_secret = settings.get('auth.cookie_secret', 'no_key_provided_for_secret')
    authn_policy = AuthTktAuthenticationPolicy(cookie_secret, callback=groupFinder)
    authz_policy = ACLAuthorizationPolicy()
    set_cache_regions_from_settings(settings)
    default_session_factory = session_factory_from_settings(settings)
    config = Configurator(
        settings=settings,
        authentication_policy=authn_policy,
        authorization_policy=authz_policy,
        root_factory=RootFactory,
        )
    config.set_session_factory(default_session_factory)
    #user logging
    config.add_tween('monitor.tween.user_log_tween.user_log_tween_factory')
    #translation
    config.add_subscriber('monitor.subscribers.add_renderer_globals', 'pyramid.events.BeforeRender')
    config.add_subscriber('monitor.subscribers.add_localizer', 'pyramid.events.NewRequest')
    config.add_translation_dirs('monitor:locale')
    #oauth
    config.include('velruse.providers.google_oauth2')
    config.add_google_oauth2_login_from_settings(prefix='google.')
    #templates
    config.include('pyramid_chameleon')
    config.include('pyramid_mako')
    #static
    production = asbool(settings.get('monitor.isproduction', 'true'))

    #devel settings (see also development.ini)
    if not production:
        global STATIC_ROUTES
        STATIC_ROUTES = {
            "static": "static_dev",
            "js": "static_dev/node_modules",
            "help_res":"static_dev/help"
        }
        for k,v in STATIC_ROUTES.iteritems():
            config.add_static_view(k,v, cache_max_age=3600)

    #production settings (see also production.ini)
    else:
        global STATIC_ROUTES
        STATIC_ROUTES = {
            "static": "static",
            "js": "static/node_modules"
        }
        for k,v in STATIC_ROUTES.iteritems():
            config.add_static_view(k,v, cache_max_age=3600)
        '''
        config.add_static_view('static', 'static', cache_max_age=3600)
        config.add_static_view('js','static/node_modules', cache_max_age=3600)
        '''
    #ordinary routes
    config.add_route('home', '/')
    config.add_route('help', '/help')
    #auth
    config.add_route("set_password", '/admin/setpassword')
    config.add_route("reset_password", '/admin/resetpassword')
    config.add_route('login', '/login')
    config.add_route('logout', '/logout')
    #translate
    config.add_route('request_new_trans', '/api/trans/new')
    config.add_route('get_translations', '/api/trans/get')

    #clinic
    config.add_route('clinic_status', '/clinic/{clinic_name}/status')
    config.add_route('csv_api', '/api/csv')
    config.add_route('couchdb_api', '/api/{db}/{design}/{view}/json')
    config.add_route('couchdb_changes', '/api/{db}/_changes')
    config.add_route('clinic_map', '/clinic/map')
    config.add_route('clinic_browse', '/clinic/browse')
    #eval
    config.add_route('network_map', '/evaluate/network')
    config.add_route('me_latency', '/evaluate/latency')
    config.add_route('me_form_type', '/evaluate/form-type')
    config.add_route('me_adherence_select', '/evaluate/adherence')
    config.add_route('me_adherence_select_reported', '/evaluate/adherence-reported')
    config.add_route('me_adherence', '/evaluate/adherence/{vaccine}')
    config.add_route('me_adherence_reported', '/evaluate/adherence-reported/{vaccine}')
    #admin
    config.add_route('lang', '/admin/user/lang/{code}')
    config.add_route('admin_users', '/admin/users')
    config.add_route('admin_users_json', '/admin/users/json')
    config.add_route('admin_user_add_json','/admin/users/add/json')
    config.add_route('usage_total', '/admin/users/usage')
    config.add_route('usage_user', '/admin/users/usage/{user_name}')
    config.add_request_method(getUser, 'user', reify=True)
    config.scan()

    return config.make_wsgi_app()
