<%inherit file="base-js-dataviz.mako"/>

<%block name="header">${parent.header()}
<title>${_("Form Type View")}</title>
<style>
</style>
</%block>


<%def name="body()">
<div class='row' ng-app='Monitor.DataViz'>

    <div class='large-10 large-order-2 medium-8-offset-1 medium-order-1 small-12 large-order-2 small-order-2 columns'>
        <div class='rounded white padded'>
            <h3>${_("Search")}</h3>
            <bar-chart charttype="line" fill=false height=600 width=900 affinity='["search"]'></bar-chart>
            <h3>${_("Registraion")}</h3>
            <bar-chart charttype="line" fill=false height=600 width=900 affinity='["registration"]'></bar-chart>
            <table-view name='form-type-table' affinity='["registration", "search"]'></table-view>
            <h3>${_("Search Types")}</h3>
            <bar-chart charttype="line" fill=false height=600 width=900 affinity='["qr code"]'></bar-chart>
            <table-view name='form-type-table' affinity='["qr code", "biometric search", "demographic"]'></table-view>
        </div>
    </div>
    <div class='large-2 large-order-1 show-for-large small-order-2 limity columns'>
        <div class='rounded white padded'>
                <drill-display data='{"data":{"x_axis_is_temporal":true, "transform":"null","axis_index":[0],"base_url":"/api/form/py-form-type/drill-date-loc-type/json","geo_start":1},"levels":{"0":{"reduce":true,"group_level":2},"1":{"reduce":true,"group_level":3},"2":{"reduce":true,"group_level":5},"3":{"reduce":true,"group_level":5}}}'></drill-display>
        </div>
    </div>
    <div id='left-toggle' class='hide-for-large columns'>
        <input type="checkbox" name="toggle" id="toggle" />
        <label for="toggle"></label>
        <div class="container">
            <div class="message">
                <drill-display data='{"data":{"x_axis_is_temporal":true, "transform":"null","axis_index":[0],"base_url":"/api/form/py-form-type/drill-date-loc-type/json","geo_start":1},"levels":{"0":{"reduce":true,"group_level":2},"1":{"reduce":true,"group_level":3},"2":{"reduce":true,"group_level":5},"3":{"reduce":true,"group_level":5}}}'></drill-display>
            </div>
        </div>
    </div>

</div>
</%def>


