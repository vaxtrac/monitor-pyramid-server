<%inherit file="base-js-widget.mako"/>
<%block name="header">${parent.header()}


<script src="/js/moment/moment.js"></script>
<script src="/js/moment-timezone/builds/moment-timezone-with-data.min.js"></script>
<script src="/static/app/streaming/formstream.js"></script>
<script src="/static/app/clinicmap/clinicmap.js"></script>

<style>
  #map {
    height: 75%;
    width: 98%;
  }
  #map-container {
    width: 80%;
    height: 70%;
  }
</style>
</%block>
<div ng-app='Monitor.Streaming.FormStream'>
    <div id='left-toggle' class='hide-for-large columns'>
        <input type="checkbox" name="toggle" id="toggle" />
        <label for="toggle"></label>
        <div class="container">
            <div class="message">
                <ul class="accordion" style="margin-top:.5em;">
                    <a href="#"class="accordion-title"><b>${_("Navigation Menu")}</b></a>
                    <ul class="submenu">
                    <a href="#"class="accordion-title"><b>${_("Project Status")}</b></a>
                    <li><a href="/clinic/map">${_("Clinic Status Map")}</a></li>
                    <li><a href="/clinic/browse">${_("Browse Clinics")}</a></li>
                    </ul>
                    <ul class="submenu">
                    <a href="#"class="accordion-title"><b>${_("Monitor & Evaluation")}</b></a>
                    <li><a href="/evaluate/adherence">${_("Schedule Adherence")}</a></li>
                    <li><a href="/evaluate/form-type">${_("Types of Forms Submitted")}</a></li>
                    <li><a href="/evaluate/latency">${_("Data Latency")}</a></li>
                    <li><a href="/evaluate/network">${_("Network Analysis")}</a></li>
                    </ul>
                    <ul class="submenu">
                    <a href="#"class="accordion-title"><b>${_("Administration")}</b></a>
                    <li><a href="/admin/users/usage">${_("Usage History")}</a></li>
                    <li><a href="/logout">${_("Logout")}</a></li>
                    </ul>
                </ul>
            </div>
        </div>
    </div>
    <div class='row uncollapse dark' >
        <div class='large-2 show-for-large min-20 columns white rounded centered'>
            <div class="container">
                <div class="message">
                    <ul class="accordion" style="margin-top:.5em;">
                        <a href="#"class="accordion-title"><b>${_("Navigation Menu")}</b></a>
                        <ul class="submenu">
                        <a href="#"class="accordion-title"><b>${_("Project Status")}</b></a>
                        <li><a href="/clinic/map">${_("Clinic Status Map")}</a></li>
                        <li><a href="/clinic/browse">${_("Browse Clinics")}</a></li>
                        </ul>
                        <ul class="submenu">
                        <a href="#"class="accordion-title"><b>${_("Monitor & Evaluation")}</b></a>
                        <li><a href="/evaluate/adherence">${_("Schedule Adherence")}</a></li>
                        <li><a href="/evaluate/form-type">${_("Types of Forms Submitted")}</a></li>
                        <li><a href="/evaluate/latency">${_("Data Latency")}</a></li>
                        <li><a href="/evaluate/network">${_("Network Analysis")}</a></li>
                        </ul>
                        <ul class="submenu">
                        <a href="#"class="accordion-title"><b>${_("Administration")}</b></a>
                        <li><a href="/admin/users/usage">${_("Usage History")}</a></li>
                        <li><a href="/logout">${_("Logout")}</a></li>
                        </ul>
                    </ul>
                </div>
            </div>
        </div>
        <div class='large-8 medium-8 small-9 min-20 columns white rounded centered'>
            <div id='map-ctrl' ng-controller='FormStreamMap'></div>
            <div id="map"></div>
        </div>
        <div class='large-2 medium-4 small-3 min-20 columns white rounded centered'>
            <ul class="accordion" style="margin-top:.5em;">
                <a href="#"class="accordion-title"><b>${_("Recent Submissions")}</b></a>
                <form-ticker name="recent-forms"></form-ticker>
            </ul>
        </div>
    </div>
</div>
<%block name="endscript">
<script>
    var map;
    function initMap() {
        var mapCtrl = angular.element(document.getElementById('map-ctrl'));
        mapCtrl.scope().$apply(function(scope){
            scope.init(document.getElementById('map'));
        });
    }
</script>
    <script src="https://maps.googleapis.com/maps/api/js?key=${google_maps_api_key}&callback=initMap"
        async defer></script>
</%block>
