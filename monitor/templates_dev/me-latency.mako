<%inherit file="base-js-dataviz.mako"/>

<%block name="header">${parent.header()}
<title>${_("Latency View")}</title>
<style>
</style>
</%block>


<%def name="body()">
<div class='row' ng-app='Monitor.DataViz'>

    <div class='large-10 large-order-2 medium-8-offset-1 medium-order-1 small-12 large-order-2 small-order-2 columns'>
        <div class='rounded white padded'>
            <bar-chart charttype="line" height=600 width=900 ></bar-chart>
            <table-view name='latency-table' ></table-view>
        </div>
    </div>
    <div class='large-2 large-order-1 show-for-large small-order-2 limity columns'>
        <div class='rounded white padded'>
                <drill-display data='{"data":{"x_axis_is_temporal":true,"transform":"average_hours_into_days","axis_index":[0],"base_url":"/api/form/python-me-latency/by-month/json","geo_start":2},"levels":{"0":{"filter":true,"reduce":true,"group_level":3},"1":{"reduce":true,"group_level":4},"2":{"reduce":true,"group_level":7},"3":{"reduce":true,"group_level":6}}}'></drill-display>
        </div>
    </div>
    <div id='left-toggle' class='hide-for-large columns'>
        <input type="checkbox" name="toggle" id="toggle" />
        <label for="toggle"></label>
        <div class="container">
            <div class="message">
                <drill-display data='{"data":{"x_axis_is_temporal":true,"transform":"average_hours_into_days","axis_index":[0],"base_url":"/api/form/python-me-latency/by-month/json","geo_start":2},"levels":{"0":{"filter":true,"reduce":true,"group_level":3},"1":{"reduce":true,"group_level":4},"2":{"reduce":true,"group_level":7},"3":{"reduce":true,"group_level":6}}}'></drill-display>
            </div>
        </div>
    </div>

</div>
</%def>


