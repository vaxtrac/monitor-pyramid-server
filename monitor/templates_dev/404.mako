<%inherit file="base.mako"/>

<div class='row'>
    <div class='large-8 large-offset-2  centered columns'>
        <div class='white rounded center padded'>
            <img src="/static/images/404.png" style='width: 90%; height: auto;'/>
            <H1><B>404: ${_("Page not found.")}</B></H1>
        </div>
    </div>
</div>


