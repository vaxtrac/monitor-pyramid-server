<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<!-- MK template header start -->
<%block name="header"/>
<!-- MK template header end -->
<link href="/static/css/app.css" rel="stylesheet" type="text/css" media="all" />
<link href="/static/css/foundation.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="/static/js/cookie.js"></script>
<script src="http://www.localeplanet.com/api/auto/icu.js"></script>
<script src="http://www.localeplanet.com/api/translate.js"></script>

<script type="text/javascript">
function googleTranslateElementInit() {
  if (!langCookieSame()){
      setLangCookie(readCookie('lang'));
  };
  if(useGoogleTranslate()){
      new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
  }else{
      console.info("Not using google machine translations");
  }
}
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<script type="text/javascript" src="/static/app/translate/translate.js"></script>

<meta name="viewport" content="initial-scale=1.0">
</head>
<body>

<div id="header" class="row">
    <div id="logo" class="small-6 medium-6 large-6 columns">
            <h1><a href="/">${_("VaxTrac Monitor")}</a></h1>
            <span>sierra leone</span>
    </div>

    <div id="header-nav" class="large-2 large-offset-2 medium-3 small-3  columns">
        <a href="/" class="button">${_("Homepage")}</a>
        <div id="google_translate_element"></div>
    </div>
    <div id="header-nav" class="large-2 medium-3 small-3 columns">
        <a href="/logout" class="button">${_("Logout")}</a></li>
    </div>

</div>
<div id="divider" class="row column bottom">
</div>
<div id="content">
${next.body()}
</div>

<!-- MK template endscript start -->
<%block name="endscript"/>
<!-- MK template endscript end -->

<div id="divider" class="row column">
</div>
<div id="copyright" class="row">
    <div class="center column">
	    <p>&copy; VaxTrac. ${_("All rights reserved.")}</p>
        <div id='collapse'>
            <label class="collapse" for="_1"><p>${_("click for open source and graphic acknowledgements")}</p></label>
            <input id="_1" type="checkbox">
            <div>
                <p>
                <a href='http://http://foundation.zurb.com/'>${_("we use foundation css")}</a></br>
                <a href='http://freepik.com'>${_("some art designed by freepik")}</a>
                </p>
            </div>
        </div>

    </div>
</div>
</body>
</html>
