<%inherit file="base.mako"/>
<div class='row'>

    <div class='large-2 columns'>
    </div>
    <div class='large-8 columns'>
        <div class='white rounded center padded'>
            <img src="/static/images/403.png" style='width: 100%; height: auto;' />
            % if failed:
                <h1>${failed}</h1>
            %else:
                <form action="${login_url}" method="get">
                    <h1>${_("You don't have permission to view this page.")}</h1>
                    <input type="submit" value="Login" />
                </form>
            % endif
        </div>
    </div>
    <div class='large-2 columns'>
    </div>
</div>

