<%inherit file="base.mako"/>
<div class='row uncollapse dark' >
    <div class='large-8 large-offset-2 min-20 columns white rounded centered'>

        % if reset_ok:
            <p class="center" style="margin-top:9vh; margin-bottom:4em;">
                <span>${_('Reset instructions have been sent to you.')}</span><br>
            </p>

        % else:
            <p class="center" style="margin-top:9vh;"><b>Reset your Password.</b></p>
            <form class="center" action="${this_url}" method="post" name="reset_form" style="margin-top:9vh; margin-bottom:4em;">
                <input type="email" placeholder="email" name="login" required>
                <input type="submit" value="${_("Reset Password")}" />
            </form>
        % endif
        % if reset_msg:
            <p class="center">
                <span>${reset_msg}</span><br>
            </p>
        % endif

</div>
