<%inherit file="base-js-widget.mako"/>

<%block name="header">${parent.header()}
<script src="/static/app/quickselect/status.js"></script>
<script src="/js/chart.js/Chart.js"></script>
<script src="/static/app/dataviz/dataviz.js"></script>
<title>${clinicName}</title>
<style>
</style>
</%block>

<div ng-app='QuickSelect.Status'>
    <div class='row'>

        <div class='large-8 large-order-2 medium-12 small-12 columns'>
            <div class='rounded white padded'>
                <clinic-table clinicid="${clinicID}">
                <clinic-table/>
                <status-select label="${_("Search / Registration Frequency")}" type="search-reg">
                </status-select>
            </div>
        </div>
        <div class='large-2 large-order-3 show-for-large columns'>

            <div class='rounded white padded'>
                <chart-legend name="search-reg">
                </chart-legend>
                <date-range>
                </date-range>
                <month-picker style="visibility: hidden;">
                <month-picker/>
            </div>

        </div>
        <div class='large-2 large-order-1 show-for-large limity columns'>
            <clinic-groups test='clinic_name' groups='["${clinicName}"]'></clinic-groups>
        </div>
        <div id='left-toggle' class='hide-for-large columns'>
            <input type="checkbox" name="toggle" id="toggle" />
            <label for="toggle"></label>
            <div class="container">
                <div class="message">
                    <clinic-groups test='clinic_name' groups='["${clinicName}"]'></clinic-groups>
                    <date-range>
                    </date-range>
                    <chart-legend name="search-reg" style="margin-top:.5em;" >
                    </chart-legend>
                    <chart-legend name="search-types" style="margin-top:.5em;">
                    </chart-legend>
                    <chart-legend name="visit-types" style="margin-top:.5em;">
                    </chart-legend>
                </div>
            </div>
        </div>

    </div>
    <div class='row'>
        <div class='large-2 large-order-1'>
        </div>
        <div class='large-8 large-order-2 medium-12 small-12 columns'>
            <div class='rounded white padded'>
                <status-select label="${_("Searches by Type")}" type="search-types">
                </status-select>
            </div>
        </div>
        <div class='large-2 large-order-3 show-for-large columns'>

            <div class='rounded white padded'>
                <chart-legend name="search-types">
                </chart-legend>
            </div>

        </div>
    </div>
        <div class='row'>
        <div class='large-2 large-order-1'>
        </div>
        <div class='large-8 large-order-2 medium-12 small-12 columns'>
            <div class='rounded white padded'>
                <status-select label="${_("Completions Vs. Outstanding")}" type="visit-types">
                </status-select>
            </div>
        </div>
        <div class='large-2 large-order-3 show-for-large columns'>

            <div class='rounded white padded'>
                <chart-legend name="visit-types">
                </chart-legend>
            </div>

        </div>
    </div>
</div>

