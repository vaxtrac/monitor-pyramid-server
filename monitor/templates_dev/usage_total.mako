<%inherit file="base-js-usage.mako"/>

<%block name="header">${parent.header()}
<title>${_("User Activity")}</title>
</%block>

<div id="banner-wrapper">
	<div id="banner" class="container"></div>
</div>
    <div ng-app='UserActivity'>
        <table class="pure-table">
            <tr>
                <th>${_("User")}</th>
                <th>${_("Last Visit")}</th>
                <th>${_("Last Page")}</th>
                <th>${_("Page Hits")}</th>
            </tr>
            <tbody all-activity></tbody>
        </table>
    </div>






