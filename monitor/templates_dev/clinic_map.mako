<%inherit file="map_zone_select.mako"/>
<%block name="header">${parent.header()}
<script src="/static/app/clinicmap/clinicmap.js"></script>
<script src="/static/app/clinicmap/activitymap.js"></script>
<title>${_("Simple Map")}</title>
<meta name="viewport" content="initial-scale=1.0">
<meta charset="utf-8">

<style>
  #map {
    height: 75%;
    width: 98%;
  }
  #map-container {
    width: 80%;
    height: 70%;
  }
</style>
</%block>
<%def name="body()">
    <div class='large-9 large-order-2 medium-12 medium-order-2 small-12 small-order-2 columns'>
        <div id='map-ctrl' ng-controller='ActivityMapController'>

        </div>
        <div id="map"></div>
    </div>
</%def>
<%block name="endscript">
<script>
    var map;
    function initMap() {
        var mapCtrl = angular.element(document.getElementById('map-ctrl'));
        mapCtrl.scope().$apply(function(scope){
            scope.init(document.getElementById('map'));
        });
    }
</script>
    <!-- new: AIzaSyA8Xxv0Pf4oeiNjcuyEhE-_8dde4fcYv_0 -->
    <!-- AIzaSyAc9IBHPtE4VD1hNH7QhuVH-oLaxtvSJHk -->
    <script src="https://maps.googleapis.com/maps/api/js?key=${google_maps_api_key}&callback=initMap"
        async defer></script>
</%block>





