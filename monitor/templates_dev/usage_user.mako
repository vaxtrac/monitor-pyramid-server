<%inherit file="base-js-usage.mako"/>

<%block name="header">${parent.header()}
<title>${_("User Activity")}</title>
</%block>

<div id="banner-wrapper">
	<div id="banner" class="container">
	</div>
</div>
<div ng-app='UserActivity' ng-controller="ActivityOffsetController as AOC">
    <table class="pure-table">
        <button ng-click="lastPage()">${_("Newer 20")}</button>
        <tr>
            <th>Timestamp</th>
            <th>Page</th>
        </tr>
        <button ng-click="nextPage()">${_("Older 20")}</button>
        <tbody all-user-activity name="'${userName}'"></tbody>
    </table>
</div>






