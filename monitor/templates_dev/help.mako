<%inherit file="base-js.mako"/>
<%block name="header">
    ${parent.header()}
</%block>

% for k in sorted(resources.keys()):
    <div class='row'>
        <div class='large-8 large-offset-2 medium-10 medium-offset-1 small-12 small-offset-0 columns'>
            <div class='white rounded center padded'>
                <h3>${resources.get(k).get("title")}</h3>
                <br>
                <img src="help_res/${url}/${k}.jpg"></img>
                <br>
                <p>${resources.get(k).get("caption")}</p>
            </div>
        </div>
    </div>
% endfor




