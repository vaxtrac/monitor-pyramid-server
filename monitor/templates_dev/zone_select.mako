<%inherit file="base-js-widget.mako"/>
<%block name="header">${parent.header()}
<meta charset="utf-8">
<style>
  #clinicidx {
    height: 75%;
    width:100%;
    overflow:auto;
  }
</style>
</%block>
<%def name="body()">
    <div id="banner-wrapper">
        <div id="banner" class="container">
        </div>
    </div>
    <div id='container' ng-app='ClinicMap'>
        <div id='left200'>
            <div id='clinicidx'>
                <clinic-groups test='dds'></clinic-groups>
            </div>
        </div>
        ${next.body()}
    </div>
</%def>





