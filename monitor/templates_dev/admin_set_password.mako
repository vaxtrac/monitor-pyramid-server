<%inherit file="base.mako"/>
<div class='row uncollapse dark' >
    <div class='large-8 large-offset-2 min-20 columns white rounded centered'>
        % if form_ok:
            <p class="center" style="margin-top:9vh;"><b>${_("Set your new password.")}</b></p>
            <form class="center" action="${this_url}" method="post" name="set_password" style="margin-bottom:4em;">
                <input type="hidden" name="token" value="${token}">
                <input type="hidden" name="login" value="${login}">
                <input type="password" placeholder="${_("new password")}" name="pw1" required><br>
                <input type="password" placeholder="${_("repeat password")}" name="pw2" required><br>
                <input type="submit" value="${_("Set Password")}" />
            </form>
        % else:
            <p class="center" style="margin-top:9vh;"><b>${form_msg}</b></p>
        % endif
</div>
