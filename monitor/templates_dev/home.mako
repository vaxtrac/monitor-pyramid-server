<%inherit file="base.mako"/>
<div class='row uncollapse dark' >
    <div class='large-4 medium-4 medium-offset-0 small-8 small-offset-2 min-20 columns white rounded centered'>
        <div onclick="location.href='/clinic/map';" class="clickable">
            <div class="title">
                <h2 >View Clinic Map</h2>
            </div>
            <p>View clinic status map.</p>
        </div>
    </div>
    <div class='large-4 medium-4 medium-offset-0 small-8 small-offset-2 min-20 columns white rounded centered'>
        <div onclick="location.href='/clinic/browse';" class="clickable">
            <div class="title">
                <h2>Browse Clinics</h2>
            </div>
            <p>View Clinic Visits by Region/Name here.</p>
        </div>
    </div>
    <div class='large-4 medium-4 medium-offset-0 small-8 small-offset-2 min-20 columns white rounded centered'>
        <div onclick="location.href='/admin/users/usage';" class="clickable">
            <div class="title">
                <h2>Administration</h2>
            </div>
            <p>View how people are using the site</p>
    </div>

    </div>


</div>

