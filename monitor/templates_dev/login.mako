<%inherit file="base.mako"/>
<div class='row uncollapse dark' >
    <div class='large-8 large-offset-2 min-20 columns white rounded centered'>
        <img style=" display:block; margin-left:auto; margin-right:auto; min-width:20em; max-width:25vw; margin-top:2em;" class="center" src="/static/images/VaxTrac_Logo_2000.png"/><br>
        <p class="center" style="margin-top:3vh;"><b>${_("Login to Monitor")}</b></p>
        <form class="center" action="${this_url}" method="post" name="login_form" style="margin-top:3vh;">
            <p>
                <input type="email" placeholder="${_('email')}" name="login" required><br>
                <input type="password" placeholder="${_('password')}" name="password" required><br>
            </p>
            <input type="submit" value="${_("Login")}" />
        </form>
        % if login_failed:
            <p class="center">
                <span>${_("Login Failed!")}</span><br>
                <a href="/admin/resetpassword">${_("forgot your password?")}</a>
            </p>
        % endif
</div>


