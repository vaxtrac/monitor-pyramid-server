<%inherit file="base-js-dataviz.mako"/>

<%block name="header">${parent.header()}
<title>${_("Dosing/ Reported Schedule Adherence View")}</title>
<style>
</style>
</%block>


<%def name="body()">
<div class='row' ng-app='Monitor.DataViz'>

    <div class='large-10 large-order-2 medium-8-offset-1 medium-order-1 small-12 large-order-2 small-order-2 columns'>
        <div class='rounded white padded'>
            <p>${_("Dose Provided at Weeks of Age.")}<p>
            <div ng-repeat="dose in ${doses}">
                <p>${vaccine} - {{dose}}</p>
                <bar-chart style="max-height: 30vh !important;" affinity=[{{dose}}] charttype="line"></bar-chart>

            </div>
            <table-view affinity=[${'"'+'","'.join([str(d) for d in doses])+'"'}] name='latency-table' ></table-view>
        </div>
    </div>
    <div class='large-2 large-order-1 show-for-large small-order-2 limity columns'>
        <div class='rounded white padded'>
                <drill-display data='{"data":{"x_axis_is_temporal":false,"has_temporal":true,"has_static":true,"static_value":"${vaccine}","temporal_index":[2],"axis_key_start":[0],"axis_key_end":[52],"axis_index":[1],"base_url":"/api/case/py-adherence-reported/drill-view/json","geo_start":3},"levels":{"0":{"filter":true,"reduce":true,"group_level":4},"1":{"filter":true,"reduce":true,"group_level":5},"2":{"filter":true,"reduce":true,"group_level":6}}}'></drill-display>
        </div>
    </div>
    <div id='left-toggle' class='hide-for-large columns'>
        <input type="checkbox" name="toggle" id="toggle" />
        <label for="toggle"></label>
        <div class="container">
            <div class="message">
                <drill-display data='{"data":{"x_axis_is_temporal":false,"has_temporal":true,"has_static":true,"static_value":"${vaccine}","temporal_index":[2],"axis_key_start":[0],"axis_key_end":[52],"axis_index":[1],"base_url":"/api/case/py-adherence-reported/drill-view/json","geo_start":3},"levels":{"0":{"filter":true,"reduce":true,"group_level":4},"1":{"filter":true,"reduce":true,"group_level":5},"2":{"filter":true,"reduce":true,"group_level":6}}}'></drill-display>
            </div>
        </div>
    </div>

</div>
</%def>


