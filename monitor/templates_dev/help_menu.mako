<%inherit file="base-js.mako"/>
<%block name="header">
    ${parent.header()}
</%block>

    <div class='row'>
        <div class='large-8 large-offset-2 medium-10 medium-offset-1 small-12 small-offset-0 columns'>
            <div class='white rounded center padded'>
                <h3>Help Topics</h3>
                <p>
                % for k in sorted(names):
                <a href="/help?page=${k}">${k}</a><br>
                % endfor
                </p>
            </div>
        </div>
    </div>
