<%inherit file="base-js-dataviz.mako"/>

<%block name="header">${parent.header()}
<title>${_("Select Vaccine for Reported Adherence View")}</title>
</%block>


<%def name="body()">
    <div class='row'>
        <div class='large-3 medium-3 columns'>
        </div>
        <div class='large-6 medium-6 small-12 columns'>
            <div class='rounded white padded'>
                <h3>Select A Vaccine to View</h3>
                <table>
                % for name in doses:
                    <tr>
                    <td><a href="/evaluate/adherence-reported/${name}">${name}</a></td>
                    </td>
                % endfor
                </table>
            </div>
        </div>
    </div>
</%def>
