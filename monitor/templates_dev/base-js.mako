## base-js.mako
<%inherit file="base.mako"/>
<%block name="header">
<script src="/js/angular/angular.js"></script>
<script src="/js/underscore/underscore.js"></script>
<script>window.underscore = _.noConflict();</script>
</%block>
${next.body()}
