<%inherit file="base-js-widget.mako"/>
<%block name="header">${parent.header()}
<meta charset="utf-8">

</%block>
<%def name="body()">
    <div class='row' ng-app='ClinicMap.Activity'>

        ${next.body()}
        <div class='show-for-large large-3 large-order-1 limity columns'>
            <div id='clinicidx'>
                <map-legend></map-legend>
                <clinic-groups test='zone'></clinic-groups>
            </div>
        </div>
        <div id='left-toggle' class='hide-for-large columns'>
            <input type="checkbox" name="toggle" id="toggle" />
            <label for="toggle"></label>
            <div class="container">
                <div class="message">
                    <map-legend></map-legend>
                    <clinic-groups test='zone'></clinic-groups>
                </div>
            </div>
        </div>
</%def>





