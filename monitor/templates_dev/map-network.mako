<%inherit file="base-js-widget.mako"/>
<%block name="header">${parent.header()}

<script src="/js/hull/hull.js"></script>
<script src="/static/app/clinicmap/clinicmap.js"></script>
<script src="/static/app/clinicmap/networkmap.js"></script>

<style>
  #map {
    height: 75%;
    width: 98%;
  }
  #map-container {
    width: 80%;
    height: 70%;
  }
</style>
</%block>
<div ng-app='ClinicMap.NetworkMap'>
    <div id='left-toggle' class='hide-for-large columns'>
        <input type="checkbox" name="toggle" id="toggle" />
        <label for="toggle"></label>
        <div class="container">
            <div class="message">

            </div>
        </div>
    </div>
    <div class='row uncollapse dark' >
        <div class='large-2 show-for-large min-20 columns white rounded centered'>
            <div class="container">
                <div class="message">
                </div>
            </div>
        </div>
        <div class='large-8 medium-12 small-12 min-20 columns white rounded centered'>
            <div id='map-ctrl' ng-controller='NetworkMap'></div>
            <div id="map"></div>
        </div>
        <div class='large-2 show-for-large min-20 columns white rounded centered'>
            <ul class="accordion" style="margin-top:.5em;">
            </ul>
        </div>
    </div>
</div>
<%block name="endscript">
<script>
    var map;
    function initMap() {
        var mapCtrl = angular.element(document.getElementById('map-ctrl'));
        mapCtrl.scope().$apply(function(scope){
            scope.init(document.getElementById('map'));
        });
    }
</script>
    <script src="https://maps.googleapis.com/maps/api/js?key=${google_maps_api_key}&callback=initMap"
        async defer></script>
</%block>
