<%inherit file="base-js-widget.mako"/>
<%block name="header">${parent.header()}
<script src="/static/app/admin/users/users.js"></script>
</style>
</%block>
<div ng-app='Monitor.Admin.Users'>
    <div class='row uncollapse dark' >
        <div class='large-8 large-offset-2 min-20 columns white rounded centered'>
            <table>
                <tr>
                    <th>${_('User')}</td>
                    <th>${_('First Name')}</td>
                    <th>${_('Last Name')}</td>
                    <th>${_('Permissions')}</td>
                    <th>${_('Save')}</td>
                    <th>${_('Undo')}</td>
                </tr>
                <tbody user-table></tbody>
            </table>
            <user-add></user-add>
        </div>
    </div>
</div>
