#import couchdb
import json
from threading import Thread
from datetime import datetime
from uuid import uuid4
from .. import couchdbapi
from pyramid.settings import asbool

DB = couchdbapi.WriteableCouch().getDataBase(couchdbapi.USAGEDB)

def logRequest(request):
    name = request.authenticated_userid
    try:
        url= request.matched_route.path.format(**request.matchdict)
        urlT = url.split("/")
        if "static" in urlT or "api" in urlT or "localhost:5984" in urlT:
            return
        doLog(name, url)
    except Exception, e2:
        return

def doLog(username, url):
    doc = {
        "_id":str(uuid4()),
        "user":username,
        "url":url,
        "timestamp":datetime.now().isoformat()
    }
    #print json.dumps(doc, indent=4)
    print DB.save(doc)


def user_log_tween_factory(handler, registry):
    if True:#asbool(registry.settings.get('do_user_log')):
        # if timing support is enabled, return a wrapper
        def user_log_tween(request):
            try:
                response = handler(request)
                if request.authenticated_userid:
                    if(request.subpath or request.view_name):
                        return response
                    t = Thread(target=logRequest, args=(request,))
                    t.start()
            except Exception,e:
                print e
                return handler(request)
            return response
        return user_log_tween
    # if timing support is not enabled, return the original
    # handler
    return handler


