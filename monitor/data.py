import StringIO
import json
import csv
from couchdbapi import CouchAPI

def getIDforClinic(name):
    args = {
        "startkey": '"'+name+'"',
        "endkey": '"'+name+'ZZ"',
        "limit": 1
        }
    view = CouchAPI.view('lookup','user','idname', **args)
    view = json.loads(view)
    return view['rows'][0]['value']

def getCSVfromChartJSON(content):

    header = content.get('labels');
    header.insert(0, '')
    print json.dumps(header, indent=4)
    rows = []
    for ds in content.get('datasets'):
        row = ds.get('data')
        row.insert(0, ds.get('label'))
        rows.append(row)
    csv_buffer = StringIO.StringIO()
    csv_writer = csv.writer(csv_buffer)
    csv_writer.writerow(header)
    for row in rows:
        csv_writer.writerow(row)
    content = csv_buffer.getvalue()
    csv_buffer.close()
    return content


    
