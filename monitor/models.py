import json
import datetime
import transaction
from uuid import uuid4

from pyramid.security import (
    Allow,
    ALL_PERMISSIONS,
    Authenticated,
    Everyone,
    unauthenticated_userid,
    )

from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
    )

from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    load_only,
    )

from zope.sqlalchemy import ZopeTransactionExtension

from passlib.apps import custom_app_context as pwd_context

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()

class Translations(Base):
    __tablename__ = 'translations'
    id = Column(Integer, primary_key=True)
    language = Column(Text)
    query = Column(Text)
    result = Column(Text)

    @classmethod
    def getByAttribute(cls, attr, value):
        try:
            attribute = cls.__dict__[attr]
            item = DBSession.query(cls).filter(attribute==value).scalar()
            #Expunge so the user item doesn't have to be bound to the session...
            DBSession.expunge(item)
            return item
        except Exception ,e:
            print e

    @classmethod
    def translationExists(cls, query, language):
        res = DBSession.query(cls).filter(cls.language==language).filter(cls.query==query).all()
        res = {trans.query: trans.result for trans in res}
        if query in res.keys():
            return True
        else:
            return False

    @classmethod
    def registeredWords(cls, language):
        print "getting existing translations"
        results = {trans.query: trans.result for trans in DBSession.query(cls).filter(cls.language==language).all()}
        #print "results \n %s" % (results,)
        if results:
            return results
        else:
            return {}

    @classmethod
    def addWord(cls, word, language):
        info = {"query":word, "language":language, "result": None}
        if Translations.translationExists(word, language): return False
        trans = Translations(**info)
        with transaction.manager:
            DBSession.add(trans)
        trans = DBSession.merge(trans)
        return True

class Passwords(Base):
    __tablename__ = 'passwords'
    id = Column(Integer, primary_key=True)
    email = Column(Text) #email
    salt = Column(Text) #uuid4 salt for this user
    pwhash = Column(Text) #password hashed with salt from salt
    reset = Column(Text) # account needs password reset

    @classmethod
    def setPassword(cls, email, password):
        pw = DBSession.query(cls).filter(cls.email==email).first()
        if not pw:
            salt = str(uuid4())
            info = {
                'salt': salt,
                'pwhash': pwd_context.encrypt(salt+password),
                'reset': "false",
                'email':email
            }
            password = Passwords(**info)
            with transaction.manager:
                DBSession.add(password)
            password = DBSession.merge(password)
            return True
        else:
            try:
                salt = pw.salt
                pw.pshash= pwd_context.encrypt(salt+password)
                pw.reset = 'false'
                transaction.commit()
                return True
            except Exception, e:
                print 'problem setting password'
                return False

    @classmethod
    def needsReset(cls, email):
        pw = DBSession.query(cls).filter(cls.email==email).first()
        if pw:
            return pw.reset == 'true'
        else:
            return False

    @classmethod
    def flagReset(cls, email):
        pw = DBSession.query(cls).filter(cls.email==email).first()
        if pw:
            pw.reset = 'true'
            transaction.commit()
            return True
        else:
            return False

    @classmethod
    def checkPassword(cls, email, password):
        pw = DBSession.query(cls).filter(cls.email==email).first()
        if not pw:
            print "password entry not found for %s" % (email)
            return False
        salt = pw.salt
        pwhash = pw.pwhash
        return pwd_context.verify(salt+password, pwhash)


class AuthTokens(Base):
    __tablename__ = 'authtoken'
    id = Column(Integer, primary_key=True)
    email = Column(Text) #email to match
    value = Column(Text) #actual token
    expiry = Column(Text) # 48 hours after creation
    used = Column(Text) #used flag. Single use tokens

    @classmethod
    def createToken(cls, email):
        #48 hours for token to be redeemed
        token_time = (datetime.datetime.now() + datetime.timedelta(days=2)).isoformat()
        info = {"value":str(uuid4()), "email":email, "expiry": token_time, "used":"false"}
        token = AuthTokens(**info)
        with transaction.manager:
            DBSession.add(token)
        token = DBSession.merge(token)
        return info

    @classmethod
    def redeemToken(cls, email, value):
        tokens = DBSession.query(cls).filter(cls.email==email).filter(cls.value==value).filter(cls.used=="false").all()
        res = {token.value: {"expiry":datetime.datetime.strptime(token.expiry, "%Y-%m-%dT%H:%M:%S.%f"), "token": token} for token in tokens}
        print res
        try:
            if not res:
                raise ValueError('no matching tokens!')
            if datetime.datetime.now() < res[value].get("expiry"): #good token!
                token = res[value].get("token")
                token.used = "true" #mark as used
                transaction.commit()
                return True
            else:
                print "%s | %s" % (res[value].get("expiry"), datetime.datetime.now()) #good token!
                print "token expired"
                return False

        except Exception,e:
            print e
            #print "couldn't evaluate %s and %s" % ( res.get(value, {}).get("expiry"), datetime.datetime.now() )
            return False

class Users(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    #info
    username = Column(Text)
    email = Column(Text)
    firstname = Column(Text)
    lastname = Column(Text)
    joined = Column(Text)
    groups = Column(Text) # access groups

    @classmethod
    def setUserAttribute(cls, userid, attr, value):
        user = DBSession.query(cls).filter(cls.username==userid).first()
        try:
            if not user:
                raise ValueError("user not found with ID %s" % userid)
            else:
                setattr(user, attr, str(value))
            transaction.commit()
            print "set %s to %s" % (attr, value)
            return True
        except Exception,e:
            print e
            return False


    @classmethod
    def getByAttribute(cls, attr, value):
        try:
            attribute = cls.__dict__[attr]
            item = DBSession.query(cls).filter(attribute==value).scalar()
            #Expunge so the user item doesn't have to be bound to the session...
            DBSession.expunge(item)
            return item
        except Exception ,e:
            print e


    @classmethod
    def userExists(cls, name):
        return DBSession.query(cls).filter(cls.username==name).scalar()

    @classmethod
    def register(cls, profile):
        username = profile.get('email')
        user = Users.userExists(username)
        if user:
            return {'success':False, "reason":"User Already Registered", 'user':user}
        info = {
            'username': username,
            'firstname':profile.get('firstname'),
            'lastname':profile.get('lastname'),
            'email':username,
            'joined': datetime.datetime.now().isoformat(),
            'groups':profile.get('groups')
        }
        user = Users(**info)
        with transaction.manager:
            DBSession.add(user)
        user = DBSession.merge(user)
        return {'success':True, "reason":"User Registered", 'user':user}

    @classmethod
    def register_oauth(cls, profile):
        accounts = profile.get('accounts')
        name = profile.get('displayName')
        username = accounts[0].get('username')
        user = Users.userExists(username)
        if user:
            return {'success':True, "reason":"User Already Registered", 'user':user}
        domain = username.split('@')[1]
        if domain != 'vaxtrac.com':
            return {"success": False, "reason":"You must have a VaxTrac Email Address to Access this system.", 'user':None}
        info = {
            'username': username,
            'firstname':name.split(' ')[0],
            'lastname':name.split(' ')[1],
            'email':username,
            'joined': datetime.datetime.now().isoformat(),
            'groups':json.dumps(['basic'])
        }
        user = Users(**info)
        with transaction.manager:
            DBSession.add(user)
        user = DBSession.merge(user)
        return {'success':True, "reason":"User Registered", 'user':user}

    @classmethod
    def getUserList(cls):
        pull = ['id', 'username', 'email', 'firstname', 'lastname', 'groups']
        _all = DBSession.query(cls).options(load_only(*pull)).all()

        return {usr.username: {term:str(getattr(usr, term)) for term in pull} for usr in _all}

    def isMember(self, groupName):
        groups = self.getGroups()
        '''
        for group in groups:
            print "%s | %s | %s" % (group, groupName, group==groupName)
        '''
        if groupName in groups:
            return True
        return False

    def getGroups(self):
        try:
            #print "found %s for user %s" % (json.loads(self.groups), self.username)
            return ["group:%s" % str(group) for group in json.loads(self.groups) if json.loads(self.groups)]

        except Exception,e:
            print "error in groupfinder %s " % e
            return None


class RootFactory(object):
    __acl__ = [ (Allow, Everyone, 'unsecured'),
                (Allow, Authenticated, 'logged'),
                (Allow, 'group:admin', 'admin'),
               (Allow, 'group:admin', 'admin'),
               (Allow, 'group:basic', 'basic'),
               (Allow, 'group:demo', 'demo'),
                (Allow, 'group:developer', ALL_PERMISSIONS) ]
    def __init__(self, request):
        pass

def groupFinder(userid, request):
    user = request.user
    if user is not None:
        return user.getGroups()
    return None

def getUser(request):
    userid = unauthenticated_userid(request)
    if userid is not None:
        return Users.getByAttribute("username", userid)


def validatePassword(rt, pkg={}):
    if rt['pw1'] != rt['pw2']:
        pkg['form_msg'] += "\nPasswords don't match."
        raise ValueError(pkg['form_msg'])
    elif len(rt['pw1']) < 8:
        pkg['form_msg'] += "\nPassword must be 8 characters long."
        raise ValueError(pkg['form_msg'])
    return True

if __name__ == "__main__":
    pass

#Index('usersids', Users.username, unique=True, mysql_length=255)
#Index('my_index', MyModel.name, unique=True, mysql_length=255)
