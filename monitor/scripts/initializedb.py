import os
import sys
import transaction
import json

from sqlalchemy import engine_from_config

from pyramid.paster import (
    get_appsettings,
    setup_logging,
    )

from pyramid.scripts.common import parse_vars

from monitor.models import (
    DBSession,
    Users,
    Base,
    Translations,
    Passwords,
    AuthTokens
    )


def usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri> [var=value]\n'
          '(example: "%s development.ini")' % (cmd, cmd))
    sys.exit(1)


def main(argv=sys.argv):
    print "setting up sqlite db"
    if len(argv) < 2:
        usage(argv)
    config_uri = argv[1]
    options = parse_vars(argv[2:])
    setup_logging(config_uri)
    settings = get_appsettings(config_uri, options=options)
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.create_all(engine)

    SETTINGS = settings

    super_user = {
        "email": SETTINGS["superuser_email"],
        "firstname":"Initial",
        "lastname":"Admin",
        "groups":json.dumps(["demo","basic","admin"])
    }

    success = Users.register(super_user)
    print "DBs Setup and User %s registered." % (SETTINGS['superuser_email'])
    success = Passwords.setPassword(SETTINGS['superuser_email'], SETTINGS['superuser_password'])
    print "Password set for user %s" % (SETTINGS['superuser_email'])

if __name__ == "__main__":
    main()
