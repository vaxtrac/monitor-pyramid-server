import os
from shutil import copyfile
import subprocess
import sys
import argparse
import json

def convert_to_jpg(path, old, new):
    print "convert %s%s.png %s%s.jpg" % (path, old, path, new)
    print os.getcwd()
    subprocess.call("convert %s%s.png %s%s.jpg" % (path, old, path, new), shell=True)


def sanitize_name(old):
    print old
    old_arg = old.replace(" ", "_")
    copyfile(old, old_arg)

def make_text_template(f, pngs):
    out = {}
    for x, png in enumerate(pngs):
        out[x] = {"title":"", "caption":""}
    json.dump(out, f)

def convert_folder(path):
    print "converting FOLDER %s" % path
    out_path = None
    pngs = []
    for obj in os.listdir(path):
        if not os.path.isdir(obj):
            base_path= os.path.abspath(obj)
            base_path = base_path.split(obj)[0]
            out_path = base_path+path+"/"
            filename, ext = os.path.splitext(obj)
            if ext == ".png":
                new = filename.replace(" ", "_")
                if new != filename:
                    sanitize_name(out_path+obj)
                    os.remove(os.path.join(out_path, obj))
                pngs.append(new)
    pngs.sort()
    for x, png in enumerate(pngs):
        convert_to_jpg(out_path, png, str(x))
        os.remove(os.path.join(out_path, png+".png"))
    if pngs:
        with open("%s%s" % (out_path, "help.json"), "w") as f:
            make_text_template(f, pngs)




def make_help(folder):
    path = folder
    for obj in os.listdir(folder):
        if os.path.isdir(obj):
            make_help(obj)
    convert_folder(path);





if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("folder", help="The folder that you want to process. It'll recurse into subfolders, so be careful!")
    args = parser.parse_args()
    make_help(args.folder)
