'use strict';

angular.module('MonitorServices.CSVService', [])
.service('CSVService', function($http, $rootScope, $log, $q){
    this.getCSV = function(json_data, filename) {
        $http.post('/api/csv', json_data).
            success(function(data, status, headers, config) {
                 var anchor = angular.element('<a/>');
                 anchor.attr({
                     href: 'data:attachment/csv;charset=utf-8,' + encodeURI(data),
                     target: '_blank',
                     download: filename+'.csv'
                 })[0].click();
            }).
            error(function(data, status, headers, config) {
                // handle error
            });
    }
});
