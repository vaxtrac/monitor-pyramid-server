'use strict';

angular.module('MonitorServices.CouchService', [])
.service('CouchService', function($http, $rootScope, $log, $q){
        this.load = function(url) {
            var deferred = $q.defer();
            $log.info('loadCases from ' + url);
            $http.get(url).
                success(function(data, status, headers, config) {
                    var resp = angular.fromJson(data);
                    var rows = resp.rows;
                    deferred.resolve(rows);
                }).
                error(function(data, status, headers, config) {
                    $log.info("FAILED");
                    deferred.reject();
                });
            return deferred.promise;
        }

        this.loadChanges = function(url) {
            var deferred = $q.defer();
            $log.info('loadCases from ' + url);
            $http.get(url).
                success(function(data, status, headers, config) {
                    var resp = angular.fromJson(data);
                    deferred.resolve(resp);
                }).
                error(function(data, status, headers, config) {
                    $log.info("FAILED");
                    deferred.reject();
                });
            return deferred.promise;

        }

        this.lastChange = function(db){
            var deferred = $q.defer();
            var req = '/api/'+db+'/_changes?limit=1&descending=true';
            var pro = this.loadChanges(req);
            pro.then(function(changes){
                $log.info(changes)
                if(changes.last_seq != null){
                    $log.info("Last sequence", changes.last_seq);
                    deferred.resolve(changes.last_seq);
                }else{
                    deferred.reject();
                }
            });
            return deferred.promise;
        }
        this.changes = function(db, since, include){
            var deferred = $q.defer();
            var req = '/api/'+db+'/_changes?since='+since+'&include_docs='+include;
            var pro = this.loadChanges(req);
            pro.then(function(changes){
                var changes_count = changes.results.length
                if(changes_count > 0){
                    $log.info("returning changes numbering", changes_count);
                    deferred.resolve(changes);
                }else{
                    deferred.reject();
                }
            });
            return deferred.promise;
        }
        
        this.clinicID = function(clinicName){
            var deferred = $q.defer();
            var req = '/api/lookup/user/idname/json?reduce=false&startkey="'+clinicName+'"&endkey="'+clinicName+'ZZ"&limit=1'
            var pro = this.load(req);
            pro.then(function(rows){
                $log.info(rows.length);
                if(rows.length > 0){
                    deferred.resolve(rows[0].value);
                }else{
                    deferred.reject();
                }
            });
            return deferred.promise;
        }
    });
