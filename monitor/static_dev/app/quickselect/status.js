'use strict';

angular.module('QuickSelect.Status', ['Monitor.DataViz', 'MonitorWidgets.Widgets', 'MonitorWidgets.Widgets.C7'])
    .directive('statusSelect', function($log, $rootScope) {
        var getTemplate = function(tElement, tAttrs){
            var type = tAttrs.type;
            $log.info(type);

            var base = "/static/app/quickselect/";
            if(type == "visit-types"){
                return base+ "select-tmp.visit-types.html"
            }
            if(type == "form-types"){
                return base+ "select-tmp.form-types.html"
            }
            if(type == "search-types"){
                return base+ "select-tmp.search-types.html"
            }
            if(type == "search-reg"){
                return base+ "select-tmp.search-reg.html"
            }

        };

        return {
            templateUrl: function(tElement, tAttrs){
                return getTemplate(tElement, tAttrs);
            },
            replace: true,
            restrict: 'E',
            scope: {
                type:"@type",
                label:"@label"

            },
            link: function(scope, elems, attrs){
                $log.info('loaded status-select');
                $log.info('type',scope.type, "label", scope.label);
                scope.rows = [];
                scope.start = null;
                scope.end = null;
                var handleSelect = function(event, info){
                    $log.info("INFO", info)
                    if (scope.end == null || scope.start == null){
                        //this scope was created since the last date was broadcast
                        //request date from widget
                        $rootScope.$broadcast('request-date', {"name":"status-widget"});
                    }
                    scope.rows = [{"clinicID":info.clinicID,"clinicName":info.clinic_name, "start":scope.start,"end":scope.end}];
                    $log.info(scope.rows);
                }

                $rootScope.$on('clinic-list-select', function(event, info){
                    handleSelect(event, info);
                });

                //only on load of list
                $rootScope.$on('clinic-list-init', function(event, info){
                    handleSelect(event, info);
                });

                var handleDate = function(data){
                    if (data.start != null){
                        scope.start = {};
                        scope.start.year = data.startParts[0];
                        scope.start.month = data.startParts[1];
                        scope.start.day = data.startParts[2];

                    }
                    if (data.end != null){
                        scope.end = {}
                        scope.end.year = data.endParts[0];
                        scope.end.month = data.endParts[1];
                        scope.end.day = data.endParts[2];
                    }

                    $log.info('set caught new date');
                    $log.info(scope.start);
                    $log.info(scope.end);
                    if(scope.rows[0] != null){
                        scope.rows[0]['start'] = scope.start;
                        scope.rows[0]['end'] = scope.end;
                    }
                }

                $rootScope.$on('date-set', function(evt,data){
                    $log.info('caught date set', data);
                    handleDate(data);
                });
            },
        };
    });

