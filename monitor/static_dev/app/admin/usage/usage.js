'use strict';

angular.module('UserActivity', ['MonitorServices.CouchService'])
    .factory('US', function() {
        return window.underscore; // assumes underscore has already been loaded on the page
    }).controller('ActivityOffsetController', function($scope, $log, UsageService){
        $scope.nextPage = function(){
            UsageService.nextData();
        };
        $scope.lastPage = function(){
            UsageService.lastData();
        };

    }).service('UsageService', function($log, $rootScope, CouchService){
        var offset = 0;
        var username = "";
        var loadData = function(offset, name){
            username = name;
            $log.info('loading data with offset ' + offset);
            CouchService.load('/api/usage/general/lastviewed/json?reduce=true&group_level=6&startkey=["'+name+'",{}]&endkey=["'+name+'"]&descending=true&limit=20&skip='+offset).then( function(d){
                var visits = [];
                d.forEach(function(r){
                    var k = r.key
                    var row = {};
                    row.page = k[5];
                    row.stamp = k[1]+"-"+k[2]+"-"+k[3]+"-"+k[4];
                    visits.push(row);
                    $log.info(r);
                });
                $rootScope.$broadcast('userDataLoaded',visits);
            });
        };
        var newData = function(step){
            offset += step;
            $log.info("new offset | " + offset);
            try{
                if (offset < 0){
                    throw "Offset Can't be negative.";
                }
                loadData(offset, username);
            }catch(err){
                offset -= step;
                $log.info(err);
            }
        }
        var lastData = function(){
            newData(-20);
        }
        var nextData = function(){
            newData(20);
        }
        this.loadData = loadData;
        this.nextData = nextData;
        this.lastData = lastData;
        this.offset = offset;
    }).directive('allActivity', function($log, CouchService) {
        return {
            templateUrl: '/static/app/admin/usage/userActivityTotal.html',
            restrict: 'EA',
            replace: true,
            link: function(scope, elems, attrs){
                scope.users = [];
                CouchService.load('/api/usage/general/pageviews/json?reduce=true&group_level=1').then( function(data){
                    data.forEach( function(row){
                        var user = {};
                        user.name = row.key[0];
                        user.visits = row.value;
                        CouchService.load('/api/usage/general/lastviewed/json?reduce=true&group_level=6&startkey=["'+user.name+'",{}]&endkey=["'+user.name+'"]&descending=true&limit=1').then( function(d){
                            var k = d[0].key;
                            user.lastPage = k[5];
                            user.last = k[1]+"-"+k[2]+"-"+k[3]+"-"+k[4];
                            //$log.info(data);
                            scope.users.push(user);
                        });
                    });
                   
                    $log.info("loaded user data"); 
                   
                });
            },
        };
    }).directive('allUserActivity', function($log, UsageService) {
        return {
            templateUrl: '/static/app/admin/usage/userActivity.html',
            restrict: 'A',
            scope: {
                name:'='
            },
            replace: true,
            controller: 'ActivityOffsetController',
            link: function(scope, elems, attrs){
                scope.$on('userDataLoaded', function(event, data){
                    $log.info("got dataupdate");
                    scope.visits = data;                    
                });
                UsageService.loadData(0, scope.name);

            },
        };
    });
