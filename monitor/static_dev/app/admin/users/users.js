'use strict';

angular.module('Monitor.Admin.Users', ['Monitor.Translate'])
    .directive('userTable', function($log, $http, $rootScope) {
        return {
            templateUrl: '/static/app/admin/users/userAdmin.html',
            restrict: 'A',
            scope: {},
            replace: true,
            link: function(scope, elems, attrs){

                var url = '/admin/users/json'
                scope.users = {};

                var load = function(){
                    $http({
                        url: url,
                        method: "GET",
                        params: {}
                        })
                    .then(function(response) {
                        scope.users = angular.fromJson(response.data)
                        $log.info('finished loading');
                    });
                }

                $rootScope.$on('reload-admin-table', function(evt, data){
                    load();
                })

                $log.info("grabbing users");
                load();
                },
        };
    }).controller('userTableController', function($scope, $log, $http){

        var url = '/admin/users/json';

        var ignore = ["id", "groups", "email"];
        var immute = ['username'];
        $scope.groups = ["demo", "basic", "admin"];

        var postCache = function(){
                    $http({
                        url: url,
                        method: "POST",
                        data: {"userid":$scope.user.username, "cache":angular.toJson($scope.cache)}
                        })
                    .then(function(response) {
                        var res = angular.fromJson(response.data);
                        var updated = res['updated'];
                        $log.info("updated", updated);
                        angular.forEach(updated, function(val, key){
                            $scope.user[key] = val;
                            $scope.cache[key] = null;
                        });

                    }, function errrorCallback(response){
                        $log.error("error saving user changes", response);
                    });
                }


        $scope.init = function(user){
            $scope.modified = false;
            $scope.cache={};
            $scope.user = user;
            $scope.fields = Object.keys(user)
                .filter(
                    function(i){
                        return ignore.indexOf(i) < 0;
                    }
                );
            $scope.editing  = {};
            $scope.resetFields();
        }

        $scope.getGroup = function(){
            var groups = angular.fromJson($scope.user.groups);
            return groups[groups.length-1]
        }

        $scope.commit = function(){
            $scope.modified = false;
            $scope.resetFields();
            postCache();
            //add actual commit;
        }

        $scope.undo = function(){
            $scope.modified = false;
            $scope.resetFields();
            $scope.resetCache();
        }

        $scope.edit = function(field){
            if (immute.indexOf(field) < 0){
                $scope.modified = true;
                $scope.editing[field] = true;
            }
        }

        $scope.unfocus = function(field){
            $log.info("unfocus", field);
            $scope.editing[field] = false;
        }

        $scope.resetFields = function(){
            angular.forEach($scope.user, function(key, val){
                $scope.editing[val] = false;
            });
        }

        $scope.resetCache = function(){
            angular.forEach($scope.user, function(key, val){
                $scope.cache[val] = null;
            });
        }

    })
    .directive('userAdd', function($log, $http, $rootScope) {
        return {
            templateUrl: '/static/app/admin/users/userAdd.html',
            restrict: 'E',
            scope: {},
            replace: true,
            link: function(scope, elems, attrs){

                var url = '/admin/users/add/json';
                scope.active = false;
                scope.cache = {"groups":"demo"};
                scope.fields = ['email', 'firstname', 'lastname', 'pw1', 'pw2'];
                scope.fieldNames = {'email':"Email", 'firstname':"First Name", 'lastname':"Last Name", 'pw1':"Password", 'pw2':"Repeat Password"}
                scope.validateMsg = null;

                var sent = false;

                scope.getType = function(fld){
                    var types = {'email':"email", 'firstname':"text", 'lastname':"text", 'pw1':"password", 'pw2':"password"}
                    return types[fld];
                }

                function validate(content){
                    for (var key in content){
                        if (content[key] == null){
                            scope.validateMsg = "Complete all fields";
                            return false;
                        }
                    }
                    if (content['pw1'] != content['pw2']){
                        scope.validateMsg = "passwords must match";
                        return false;
                    }
                    if (content['pw1'].length < 8){
                        scope.validateMsg = "passwords must be 8 characters";
                        return false;
                    }
                    return true;
                }

                var postCache = function(){
                    if (!validate(scope.cache)){return;}
                    $http({
                        url: url,
                        method: "POST",
                        data: {"cache":angular.toJson(scope.cache)}
                        })
                    .then(function(response) {
                        var res = angular.fromJson(response.data);
                        var updated = res['updated'];
                        $log.info("updated", updated);
                        scope.cancel();
                        scope.sent = false;
                        $rootScope.$broadcast('reload-admin-table', null);
                        scope.validateMsg = "User Created!";

                    }, function errrorCallback(response){
                        scope.sent=false;
                        $log.error("error saving user changes", response);
                        scope.validateMsg = "Error Saving user changes";
                    });
                }

                scope.save = function(){
                    if (sent) {return;} // no double sends please...
                    if (!validate(scope.cache)){return;}
                    scope.validateMsg = null;
                    scope.sent=true;
                    postCache();
                    $log.info(scope.cache);
                }

                scope.cancel = function(){
                    scope.active = false;
                    scope.validateMsg = null;
                    scope.cache = {"groups": "demo"};
                }
            }

        };
    });
