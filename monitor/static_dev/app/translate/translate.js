'use strict';
angular.module('Monitor.Translate', [])
.factory('JSON', function() {
        return window.JSON; // assumes moment has already been loaded on the page
    })
.service('TranslateService', function($log, $q, $window, $timeout, $http, JSON){

    var started = false;
    var loaded = false;
    var locale = "en";
    var terms = {};
    var get_url = "/api/trans/get";
    var new_url = "/api/trans/new";

    var init_promise = null;

    function init(){
        if (!started){
            try{
                started = true;
                //locale = $window.readCookie('lang');
                $log.info("set locale", locale);
                init_promise = loadTerms(locale);
            }catch (e){
                $log.error(e);
            }
        }
        return init_promise;
    }


    function loadTerms(loc){
        var tsk = $q.defer();
        $log.info("loading terms" , get_url, loc, locale);
        $http({
            url: get_url,
            method: "GET",
            params: {"lang": loc}
            })
        .then(function(response) {
            terms = angular.fromJson(response.data)
            $log.info('finished loading');
            loaded = true;
            $log.info("got translations", response);
            tsk.resolve()
        });
        return tsk.promise
    }

    function registerString(new_string){
        $log.info("registering unknown string", new_string);
        $http({
            url: new_url,
            method: "POST",
            data: {"lang": locale, "query": new_string},
            headers: {'Content-Type': 'application/json'},
            })
        .then(function(response) {
            terms = response.data
            $log.info("registered new string", new_string, response);
        });
    }

    function getTerm(term){
        //$log.info("looking for", term);
        if (terms[term] != null){
            return terms[term]
        }else if( term in terms){
            //key but no translation
            return term;
        }else{
            //key missing
            registerString(term);
            return term;
        }
    }

    function getTranslation(term){
        var tsk = $q.defer();
        if (loaded){
            tsk.resolve(getTerm(term));
        }else{
            init().then( function(data){
                //$log.info('not loaded');
                tsk.resolve(getTerm(term));
            });
            //$log.info("getTranslation called before it was ready");
        }
        //$log.info('returning promise');
        return tsk.promise;
    }
    function isReady(){
        return loaded;
    }

    this.init = init;
    this.isReady = isReady;
    this.getTranslation = getTranslation;

}).filter('translate', function(TranslateService, $log){
    var val = {};
    var loaded = false;

    function getTerm(term){
        return val[term] == null ? term : val[term];
    }

    var transAsync = function(term){
        if (val[term] == null){
            if(!loaded){
                loaded = true;
                TranslateService.init().then(function(){
                    TranslateService.getTranslation(term).then(function(out){
                        $log.info("setting", term, out);
                        val[term] = out;
                    });
                });
            }
            else{
                TranslateService.getTranslation(term).then(function(out){
                    $log.info("setting get transterm", term, out);
                    val[term] = out;
                });
            }
            return term;
        }
        else return getTerm(term);
    }
    transAsync.$stateful = true;
    return transAsync;
});
