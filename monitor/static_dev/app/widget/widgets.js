'use strict';

angular.module('MonitorWidgets.Widgets', ['MonitorServices.CouchService'])
    .factory('US', function() {
        return window.underscore; // assumes underscore has already been loaded on the page
    })
    .directive('clinicGroups', function(CouchService, $rootScope, $log){
        return {
            templateUrl: '/static/app/widget/clinicGroups.html',
            restrict: 'E',
            replace: true,
            scope: {
                group:'@groups',
                test:'@test',
                values:'='
            },
            link: function(scope, elems, attrs){
                try{
                    scope.groups = JSON.parse(scope.group);
                }catch(err){
                    scope.groups = [];
                }
                if (scope.groups.length == 0){
                    CouchService.load('/api/lookup/'
                        +'python-group/clinic-group-count/json?reduce=true&group_level=2')
                        .then(
                            function(retData){
                                retData.forEach(function(row){
                                    scope.group= [];
                                    if (row.key[0] != null){
                                        scope.groups.push(row.key[1])
                                    }
                                });
                            });
                }

                scope.checked = null;
                scope.check = function(zone){
                    if(scope.checked == zone){
                        scope.checked = null;
                    }else{
                        scope.checked = zone;
                    }

                }
                scope.isChecked = function(zone){
                    if(scope.checked==null){
                        return false;
                    }
                    if (zone == scope.checked){
                        return true;
                    }
                };
            },
        };
    }).directive('clinicList', function($log, CouchService, US, $rootScope) {
        return {
            templateUrl: '/static/app/widget/clinicList.html',
            replace: true,
            restrict: 'E',
            scope: {
                test:'=',
                value:'='
            },
            link: function(scope, elems, attrs){
                $log.info("testing for value in test_category", scope.value, scope.test);
                scope.markers = [];
                var packLocation = function(row){
                    return row.value
                }

                var filter = function(item){
                    if(item[scope.test] == scope.value){
                        return true;
                    }
                    return false;
                }
                CouchService.load('/api/lookup/'
                    +'python-group/clinicid-info/json?reduce=false')
                    .then(
                        function(retData){
                            retData.forEach(function(row){
                                var item = packLocation(row);
                                if(filter(item)==true){
                                    scope.markers.push(item);
                                }
                            });
                            //Sort markers with chained _ sortby
                            scope.markers = US(scope.markers).chain()
                            .sortBy(function(marker) {
                                return marker.clinic_name;
                            }).sortBy(function(marker) {
                                return marker.group_name;
                            }).value();
                            if (scope.markers[0] != null){
                                $rootScope.$broadcast("clinic-list-init", scope.markers[0]);
                            }else{
                                $log.info("No clinics returned for criteria.")
                            }
                        });
            },
        };
    }).controller('ClinicSelectController', function($scope, $log, $rootScope){
        var info = null;
        var update = null;
        var marker = null;
        this.init = function(data){
            info = data;
            //$log.info('init clinic', info.clinicID);
            $scope.$on("label-update-" + info.clinicID, function(event, data){
                //$log.info ("update for " + info.clinicID);
                update = data;
            });
            $rootScope.$broadcast("label-update-request-" + info.clinicID, null);
        }


        this.getIcon = function(){
            if (update == null){
                return "/static/mapicons/green.png";
            }
            else{
                return update.iconUrl;
            }
        }

        this.hasIcon = function(){
            if (update == null){
                return false;
            }
            return true;
        }

        this.getLabel = function(){
            return info.group_name.split(" group")[0] + " | " + info.clinic_name;
        }
        this.center = function(){
            $rootScope.$broadcast("clinic-list-select", info);
        }

    }).directive('clinicTable', function(CouchService, $rootScope, $log, US, $window){
        return {
            templateUrl: '/static/app/widget/clinicTable.html',
            restrict: 'E',
            replace: false,
            scope: {},
            link: function(scope, elems, attrs){

                scope.vaccines = [
                    {
                        'name':'BCG',
                        'antigen':'BCG',
                        'doses':[0],
                        'doseNames':["1st"],
                        'type':'antigenCell'
                    },
                    {
                        'name':'Penta',
                        'antigen':'DTwPHibHep',
                        'doses':[0,1,2],
                        'doseNames':["1st", "2nd", "3rd"],
                        'type':'antigenCell'
                    },
                    {
                        'name':'OPV',
                        'antigen':'OPV',
                        'doses':[0,1,2,3],
                        'doseNames':["Birth","1st", "2nd", "3rd"],
                        'type':'antigenCell'
                    },
                                        {
                        'name':'IPV',
                        'antigen':'IPV',
                        'doses':[0],
                        'doseNames':["1st"],
                        'type':'antigenCell'
                    },
                    {
                        'name':'Pneumo',
                        'antigen':'Pneumo_conj',
                        'doses':[0,1,2],
                        'doseNames':["1st", "2nd", "3rd"],
                        'type':'antigenCell'
                    },
                                        {
                        'name':'Rota',
                        'antigen':'Rota',
                        'doses':[0,1],
                        'doseNames':["1st", "2nd"],
                        'type':'antigenCell'
                    },
                    {
                        'name':'Measles',
                        'antigen':'Measles',
                        'doses':[0,1],
                        'doseNames':["9M", "15M"],
                        'type':'antigenCell'
                    },
                    {
                        'name':'Fully Immunized (FIC)',
                        'antigen':'completion',
                        'doses':[0],
                        'doseNames':["1st"],
                        'type':'completion'
                    },
                    {
                        'name':'Vitamin A',
                        'antigen':'VitaminA',
                        'doses':[0, 1],
                        'doseNames':["1st", "2nd"],
                        'type':'antigenCell'
                    },
                    {
                        'name':'Yellow Fever',
                        'antigen':'YF',
                        'doses':[0],
                        'doseNames':["1st"],
                        'type':'antigenCell'
                    },

                ];
                scope.year = null;
                scope.month= null;
                scope.clinic = null;
                scope.clinicid = null;
                scope.doseLabels = [];
                scope.colLabels = [];
                scope.antigens = [];
                scope.antigenRows = [];
                scope.vaccineRow = [];
                scope.doseLabelRow = [];
                scope.colBreaks = [];
                scope.longestCol = 0;
                scope.width = $window.innerWidth;
                scope.lines = 1;

                scope.getDoseLabels = function(){
                    $log.info('getDoseLabels');
                    scope.doseLabelRow = [];
                    scope.vaccineRow.forEach(function(vaccines){
                        scope.doseLabelRow.push(
                            US.chain(vaccines)
                                .filter(function (row){
                                    return row.doses.length >1;
                                })
                                .map(function(row){
                                    return row.doseNames;
                                })
                                .flatten()
                                .map(function(val){
                                    if (Number.isInteger(val)) {
                                        return val+1;
                                    }
                                    else{ return val}
                                })
                                .value()
                            );
                        });

                }
                scope.getColLabels = function(){
                    var count = getDoseCount();
                    return Array.apply(null, Array(count)).map(Number.prototype.valueOf,0);
                }

                var getDoseCount = function(){
                    var doses = US.chain(scope.vaccines)
                            .map(function(row){
                                return row.doses;
                            })
                            .flatten()
                            .value()
                    return doses.length;
                }

                var splitArray = function(arr, breakPoints) {
                    if (!angular.isUndefined(arr) && arr.length > 0) {
                        var arrayToReturn = [];
                        var subArray=[];
                        $log.info("splitting array");
                        $log.info(arr, breakPoints);
                        for (var i=0; i<arr.length; i++){
                            if(US.contains(breakPoints, i)){
                                $log.info("pushing value into new subarray", i);
                                subArray.push(arr[i]);
                                arrayToReturn.push(subArray);
                                subArray = [];
                            }else{
                                subArray.push(arr[i]);
                            }
                        }
                        if (subArray.length > 0){ arrayToReturn.push(subArray);}
                        $log.info("splitting done", arrayToReturn);
                        return arrayToReturn;
                    }
                }

                scope.getVaccineRows = function(){
                    scope.vaccineRow = [];
                    scope.antigenRows.forEach(function(row){
                        $log.info("working on row:", row)
                        var out_row = [];
                        var antigens = US.chain(row)
                            .pluck("antigen")
                            .unique()
                            .value()
                        antigens.forEach(function(antigen){
                            out_row.push(US.where(scope.vaccines, {"antigen": antigen})[0])
                        });
                        $log.info("out_row", out_row);
                        scope.vaccineRow.push(out_row);

                    })
                }

                scope.getColBreaks = function(){
                    var lines = scope.lines;
                    var count = getDoseCount();
                    var stop = (count - (count % lines) )/lines
                    var breaks = [];
                    var cur = 0;
                    var agg = 0;
                    var lines = [];
                    scope.vaccines.forEach(function(vax){
                        if (cur >= stop){
                            agg = agg + cur
                            lines.push(cur);
                            breaks.push(agg -1);
                            cur = 0;
                        }
                        cur = cur + vax.doses.length;
                    });
                    try{
                        scope.colBreaks = breaks;
                        if(scope.colBreaks.length > 0){
                            scope.longestCol = US.max(lines);
                        }
                    }catch(err){
                        scope.colBreaks = [];
                        scope.longestCol = count;
                    }

                    $log.info("colbreaks", count, scope.colBreaks, scope.longestCol);
                    return scope.colBreaks;
                }

                scope.getAntigens = function(){
                    $log.info('getAntigens');
                    return US.chain(scope.vaccines)
                        .map(function(row){
                            var out = [];
                            row.doses.forEach(function(dose){
                                out.push({"dose": dose, "antigen": row.antigen, "type": row.type})
                            })
                            return out;
                        })
                        .flatten()
                        .value()

                }

                scope.change_month = function(inc){
                    var req = {
                        'request-type':'change-date',
                        'change-request':{
                            'field': "month",
                            "increment": inc
                            }
                    }
                    $rootScope.$broadcast('request-month', req);
                }


                scope.update_views = function(){
                    scope.colLabels = scope.getColLabels();
                    scope.antigens = scope.getAntigens();
                    scope.getColBreaks();
                    scope.antigenRows = splitArray(scope.antigens, scope.colBreaks);
                    scope.getDoseLabels();
                    scope.getVaccineRows();
                }

                scope.update = function(){
                    scope.update_views()
                    scope.$broadcast('update-cells', {'clinicid': scope.clinicid, 'start': scope.start, 'end': scope.end});
                }




                function setSize(manual){
                    var step = 600;
                    var x = scope.width;
                    var old_width = scope.lines;
                    if(scope.width < 1800){
                        scope.lines = 4 -(((x - (x % step))/step));
                    }else{
                        scope.lines = 1;
                    }
                    if (old_width != scope.lines){
                        $log.info(scope.width, scope.lines);
                        if(!manual){
                            $log.info("view changed enough for update!");
                            scope.update();
                        }
                    }


                }

                function onResize(){
                    if (scope.width !== $window.innerWidth)
                        {
                            scope.width = $window.innerWidth;
                            setSize(false);

                        }
                };

                function cleanUp() {
                    angular.element($window).off('resize', onResize);
                    angular.element($window).off('orientationchange', onResize);
                }


                angular.element($window).on('orientationchange', onResize);
                angular.element($window).on('resize', onResize);
                scope.$on('$destroy', cleanUp);

                $rootScope.$on('clinic-list-select', function(evt, data){
                    $log.info("caught", evt, data);
                    scope.clinic = data.clinic_name;
                    scope.clinicid = data.clinicID;
                    scope.update();
                });

                $rootScope.$on('clinic-list-init', function(evt, data){
                    $log.info("caught", evt, data);
                    scope.clinic = data.clinic_name;
                    scope.clinicid = data.clinicID;
                    scope.update();
                });

                $rootScope.$on('month-set', function(evt, data){
                    $log.info("caught", evt, data);
                    scope.start = data.start;
                    scope.end = data.end;
                    scope.update();
                });

                setSize(true);
                scope.getColBreaks();

                $rootScope.$broadcast('request-month', null);


            },
        };
    }).directive('antigenCell', function(CouchService, $rootScope, $log, US){
        return {
            templateUrl: '/static/app/widget/antigenCell.html',
            restrict: 'E',
            replace: true,
            scope: {
                dose:'=',
                clinicid:'=',
                antigen:'=',
                month:'=',
                year:'='
            },
            link: function(scope, elems, attrs){

                scope.count = "~";
                scope.click = function(){
                }

                function load(){
                    var month ={"start":0, "end":0}; var day = {"start":0, "end":0};
                    month['start'] = scope.start.month < 10 ? "0"+ scope.start.month: scope.start.month;
                    month['end'] = scope.end.month < 10 ? "0"+ scope.end.month: scope.end.month;
                    day.start = scope.start.day < 10 ? "0"+ scope.start.day: scope.start.day;
                    day.end = scope.end.day < 10 ? "0"+ scope.end.day: scope.end.day;


                    var req = '/api/case/nepal-report-py/weeks/json?reduce=true&group_level=6&startkey=["'+
                        scope.antigen+'","'+
                        scope.dose+'","'+
                        scope.clinicid+'","'+
                        scope.start.year+'","'+
                        month.start+'","'+
                        day.start+'"'+
                        ']&endkey=["'+
                        scope.antigen+'","'+
                        scope.dose+'","'+
                        scope.clinicid+'","'+
                        scope.end.year+'","'+
                        month.end+'","'+
                        day.end+'"'+
                        ']&descending=false&filter=true';

                    CouchService.load(req)
                    .then(function(retData){
                        if(retData[0] == null){
                            scope.count = 0;
                            return;
                        }

                        var count = US.chain(retData)
                                .map(function(row){
                                    return row.value;
                                })
                                .reduce(function(memo, pairs){
                                    var nums = US.map(US.values(pairs), function(val){
                                        return val;
                                    })
                                    var num = US.reduce(nums, function(mm, n){ return mm + n; }, 0);
                                    return memo + num; }, 0)
                                .value();
                            scope.count = count;
                    });
                }
                scope.$on('update-cells', function(evt,data){
                    scope.count = '~';
                    scope.clinicid = data.clinicid;
                    scope.start = data.start;
                    scope.end = data.end;
                    if (scope.clinicid != null){
                        load();
                    }

                })


            },
        };
    }).directive('completionCell', function(CouchService, $rootScope, $log, US){
        return {
            templateUrl: '/static/app/widget/completionCell.html',
            restrict: 'E',
            replace: true,
            scope: {
                clinicid:'=',
                month:'=',
                year:'='
            },
            link: function(scope, elems, attrs){

                scope.count = "~";
                scope.click = function(){
                    $log.info(scope.clinicid, scope.year, scope.month);
                }
                function load(){

                    var month ={"start":0, "end":0}; var day = {"start":0, "end":0};
                    month['start'] = scope.start.month < 10 ? "0"+ scope.start.month: scope.start.month;
                    month['end'] = scope.end.month < 10 ? "0"+ scope.end.month: scope.end.month;
                    day.start = scope.start.day < 10 ? "0"+ scope.start.day: scope.start.day;
                    day.end = scope.end.day < 10 ? "0"+ scope.end.day: scope.end.day;

                    var req = '/api/case/sl-completion/all/json?reduce=true&group_level=4&startkey=["'+
                        scope.clinicid+'","'+
                        scope.start.year+'","'+
                        month.start+'","'+
                        day.start+'"'+
                        ']&endkey=["'+
                        scope.clinicid+'","'+
                        scope.end.year+'","'+
                        month.end+'","'+
                        day.end+'"'+
                        ']&descending=false';

                    CouchService.load(req)
                    .then(function(retData){

                        if(retData == null || retData[0] == null){
                            $log.info("no data for", req);
                            scope.count = 0;
                            return;
                        }
                        var count = US.chain(retData)
                                .map(function(row){
                                    return row.value;
                                })
                                .reduce(function(mm, n){ return mm + n; }, 0)
                                .value();
                        scope.count = count;

                    });


                }
                scope.$on('update-cells', function(evt,data){
                    scope.count = '~';
                    $log.info('new info!');
                    scope.clinicid = data.clinicid;
                    scope.start = data.start;
                    scope.end = data.end;
                    if (scope.clinicid != null){
                        load();
                    }
                })


            },
        };
    });

