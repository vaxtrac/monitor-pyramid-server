'use strict';

angular.module('MonitorWidgets.Widgets.C7', ['MonitorServices.CouchService', 'Monitor.DataViz'])
    .factory('US', function() {
        return window.underscore; // assumes underscore has already been loaded on the page
    })
    .service('cSevenService', function(CouchService, $rootScope, $log, US){

        var reportFromDoc = function(doc){
            var data = {};
            data['labels'] = US.chain(JSON.parse(doc.properties.categories))
                .map(function(row){
                    var m1 = Math.round(row[0] /((365)/12)) ;
                    var m2 = Math.round(row[1] /((365)/12)) ;
                    return String(m1) + "M -> " + String(m2) + "M";
                })
                .value()

            data['datasets'] = US.chain(JSON.parse(doc.properties.report_data))
                .map(function(row, key){
                    return US.map(row, function(res, sub){
                        var name = key+"-"+sub;
                        $log.info(name, res);
                        var out = {};
                        out[name] = res;
                        return out;
                    });
                })
                .flatten()
                .reduce(function(res, val){
                    $log.info(res, val);
                    var key = Object.keys(val)[0];
                    if (res == null){
                        res = {}
                    }
                    res[key] =val[key];
                    return res;

                })
                .value()

            return data;
        }

        this.getDates = function(clinicID){
            $log.info('getting dates for ' + clinicID);
            var url = '/api/case/monthly-report/by-clinic/json?reduce=false&startkey=["'+clinicID+'"]&endkey=["'+clinicID+'",{}]';
            var query = CouchService.load(url);
            query.then(function(qData){
                var data = US.chain(qData)
                    .map(function(row){
                        return {"key":row.key,"name": row.key[1] +"-" + row.key[2] };
                    })
                    .value();
                $rootScope.$broadcast("c7-dates", data);
            });
        }

        this.getReport = function(key){
            var url = '/api/case/monthly-report/by-clinic/json?reduce=false&limit=1&include_docs=true&startkey=["'+key[0]+'","'+key[1]+'","'+key[2]+'"]&endkey=["'+key[0]+'","'+key[1]+'","'+key[2]+'",{}]';
            var query = CouchService.load(url);
            query.then(function(qData){
                $log.info('c7 report raw', qData);
                $rootScope.$broadcast("c7-report", reportFromDoc(qData[0].doc));
            });

        }
    })
    .directive('cSeven', function($rootScope, $log, cSevenService){
            return {
            templateUrl: '/static/app/widget/c7.html',
            restrict: 'E',
            replace: true,
            scope: {
                clinicID:'@clinicid',
            },
            link: function(scope, elems, attrs){
                cSevenService.getDates(scope.clinicID);
                scope.activeDate = 0;
                scope.activeItem = null;
                scope.dates = [{"name":"a"},{"name":"b"},{"name":"c"}];

                var loadReport = function(key){
                    cSevenService.getReport(key);
                }

                scope.next = function(){
                    if (scope.activeDate +1 < scope.dates.length){
                        scope.activeDate +=1;
                        scope.activeItem = scope.dates[scope.activeDate];
                        loadReport(scope.activeItem.key);
                    }
                }

                scope.previous = function(){
                    if (scope.activeDate -1 >= 0){
                        scope.activeDate -=1;
                        scope.activeItem = scope.dates[scope.activeDate];
                        loadReport(scope.activeItem.key);
                    }
                }

                $rootScope.$on('c7-dates', function(event,data){
                    $log.info("c7-dates",data);
                    scope.dates = data;
                    scope.activeDate = data.length-1;
                    scope.activeItem = scope.dates[scope.activeDate];
                    loadReport(scope.activeItem.key);
                });

                $rootScope.$on('c7-report', function(event,data){
                    $log.info("c7-report",data);
                    data["source"] = "c7";
                    $rootScope.$broadcast('set-table-data',data);
                });

            },
        };
    });
