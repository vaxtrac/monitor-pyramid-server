'use strict';

angular.module('Monitor.DataViz', ['MonitorServices.CouchService', 'MonitorServices.CSVService', 'Monitor.Translate'])
    .factory('Chart', function(){
        return window.Chart;
    })
    .factory('Math', function(){
        return window.Math;
    })
    .factory('US', function() {
        return window.underscore; // assumes underscore has already been loaded on the page
    })
    .service('ColorService', function($log, Math){
        this.HSVtoRGB = function(h, s, v) {
            //$log.info('Getting HSV');
            var r, g, b, i, f, p, q, t;
            i = Math.floor(h * 6);
            //$log.info(i + " " + h)
            f = h * 6 - i;
            p = v * (1 - s);
            q = v * (1 - f * s);
            t = v * (1 - (1 - f) * s);
            //$log.info([r, g, b, i, f, p, q, t]);
            switch (i % 6) {
                case 0: r = v, g = t, b = p; break;
                case 1: r = q, g = v, b = p; break;
                case 2: r = p, g = v, b = t; break;
                case 3: r = p, g = q, b = v; break;
                case 4: r = t, g = p, b = v; break;
                case 5: r = v, g = p, b = q; break;
            }
            //$log.info(r+ " | "+ g + " | "+ b);
            return {
                r: Math.round(r * 255),
                g: Math.round(g * 255),
                b: Math.round(b * 255)
            };
        }
        this.getComplementaryColors = function(seed){
            var golden_ratio_conjugate = 0.618033988749895;
            seed = seed*golden_ratio_conjugate;
            var h = seed + golden_ratio_conjugate;
            //$log.info(h);
            h = h%1;
            //$log.info(h);
            return this.HSVtoRGB(h,0.55, 0.95);
        }

        this.getRGBAString = function(seed, alpha){
            var c = this.getComplementaryColors(seed);
            //$log.info(c);
            return "rgba("+c.r+","+c.g+","+c.b+","+alpha+")";
        }

        this.getRGBASets = function(sets, colors, start, alpha){
            //Add offset to get a nice colorset
            start +=5;
            //alpha = 1.0;
            $log.info("starting triplets")
            var out = [];
            var c = 0;
            while(c < sets){
                var trip = [];
                for(var i=0; i<(colors+1); i++){
                    trip.push(this.getRGBAString(c+i+start, alpha))
                }
                out.push(trip);
                c+=1;
            }
            return out;
        }

    })
    .service('DrillService', function($log, US, $q, $rootScope, CouchService, $httpParamSerializer, Math){
        var levels = {};
        var levels_raw = {};
        var active_process = 0;
        var basic = {};
        var transform = null;
        var current_level = 0;
        var level_selection_data = {};

        $rootScope.$on("notify-drill", function(event, data){
            $log.info("drill service caught data!");
            $log.info(data);
            if(data != null){
                if(levels[current_level+1] != null){
                    current_level +=1;
                    level_selection_data[current_level] = data.value;
                    loadLevel(current_level);
                }
            }else{
                $log.info("caught up");
                current_level -=1;
                loadLevel(current_level);
            }
        });



        var default_value_transform = function(value){
            return value;
        }

        var custom_value_transform = null;

        //needs to be set by master service
        var set_custom_value_transform = function(func){
            custom_value_transform = func;
        }

        var set_transform = function(name){
            var trans = null;
            $log.info("setting transform:", name);
            if(name == "average_hours_into_days"){
                trans = function(value){
                    if(value.sum != null && value.count != null){
                        return Math.round(value.sum / value.count * (100/24)) / 100;
                    }else{
                        return 0;
                    }
                }
            }
            if(name == "average"){
                trans = function(value){
                    if(value.sum != null && value.count != null){
                        return Math.round(value.sum / value.count);
                    }else{
                        return 0;
                    }
                }
            }
            set_custom_value_transform(trans);
        }

        var formatUrl = function(level){
            $log.info("formatting Url for level", level, basic);
            var s_comp = [];
            var e_comp = [];
            var c = 0;
            //X axis is year/months/days
            if(basic.has_static){
                s_comp.push(basic.static_value);
                e_comp.push(basic.static_value);
                c+=1;
            }
            if(basic.x_axis_is_temporal){
                basic.axis_index.forEach(function(i){
                    s_comp.push(basic.start);
                    e_comp.push(basic.end);
                    c++;
                });
            }else{
                //X axis is something else but temporal elements are important
                if(basic.has_temporal){
                    var max_c = basic.temporal_index.length + basic.axis_index.length + c;
                    var c_start = c;
                    var l = 0;
                    do{
                        //axis before temporal or already have temporal
                        if(basic.temporal_index[0]  <= basic.axis_index[0] || c > c_start){
                            if(c>= max_c){continue;}
                            basic.temporal_index.forEach(function(val, i){
                                s_comp.push(basic.start);
                                e_comp.push(basic.end);
                                c++;
                            });
                        }
                        if(basic.axis_index[0] < basic.temporal_index[0] || c > c_start){
                            if(c>= max_c){continue;}
                            basic.axis_index.forEach(function(val, i){
                                s_comp.push(basic.axis_key_start[i]);
                                e_comp.push(basic.axis_key_end[i]);
                                c++;
                            });
                        }
                    }
                    while(c < max_c);
                }else{
                    //Only non-temporal elements are used in the key
                    basic.axis_index.forEach(function(val, i){
                        s_comp.push(basic.axis_key_start[i]);
                        e_comp.push(basic.axis_key_end[i]);
                        c++;
                    });
                }
            }
            level["startkey"] = "$SK";
            level["endkey"] = "$EK";
            var s_out = [];
            var e_out = [];
            s_comp.forEach(function(k){
                if (US.isNumber(k)){
                    s_out.push(k);
                }else{
                    s_out.push('"'+k+'"');
                }
            });
            e_comp.forEach(function(k){
                if (US.isNumber(k)){
                    e_out.push(k);
                }else{
                    e_out.push('"'+k+'"');
                }
            });
            var sK = '[' +s_out.join(",")+']';
            var eK = '[' +e_out.join(",")+',{}]';
            var url = $httpParamSerializer(level);
            url = url.replace("$SK", sK);
            url = url.replace("$EK", eK);
            url = basic.base_url + "?"+ url;
            $log.info("out url:" , url);
            return url;
        }

        var init = function(levelData, start_level){
            basic = levelData.data;
            transform = levelData.data.transform;
            if(transform != null){
                set_transform(transform);
            }
            levels = levelData.levels;
            $log.info("init of DrillService");
            $log.info(levels);
            loadLevel(start_level);

        };

        var xFormData = function(qData){
            //$log.info(qData);
            var out = {};
            var x_axis = basic.axis_index;
            var geo_start = basic.geo_start;
            var group_level = levels[current_level].group_level
            //generate axis values from keys
            var axis = null;
            if(x_axis.length > 1){
                //Complex x-axis. Grab unique combinations in form [a,b] -> a-b
                axis = US.chain(qData)
                    .map(function(row){
                        return row.key.slice(x_axis[0], x_axis.length);
                    })
                    .uniq(function(item){return item.join("-")})
                    .sortBy(function(item){return item.join("-")})
                    .value();
            }else{
                //simple x-axis
                if(!basic.x_axis_is_temporal){
                    //axis should be range
                    axis = US.range(parseInt(basic.axis_key_start[0], 10),1+parseInt(basic.axis_key_end[0],10),1);
                }else{
                    //axis is single value but not of a range
                    axis = US.chain(qData)
                        .map(function(row){
                            return String(row.key.slice(x_axis[0], x_axis.length));
                        })
                        .sortBy(function(item){return item;})
                        .uniq()
                        .map(function(row){
                                return [row];
                            })
                        .value();
                }

            }
            $log.info("axis", axis);
            // remove geo-combinations not in selected range
            var select_comb = level_selection_data[current_level];
            if(select_comb != null){
                if(Array != select_comb.constructor){
                    $log.info("not an array!");
                    select_comb = [select_comb];
                }
                $log.info("selection criteria:", select_comb);
                qData = US.chain(qData)
                    .filter(function(row){
                        if(US.intersection([select_comb], row.key).length > 0) {
                            $log.info(US.intersection(select_comb, row.key).length, select_comb, row.key);
                        }
                        return US.intersection(select_comb, row.key).length >= select_comb.length;
                    })
                    .value()
                //$log.info(qData);
            }else{
                $log.info("no selection criteria for level: " + current_level);
            }
            //find unique geo combinations from remaining section of key
            var keys = null;
            $log.info("group_level, geo_start", group_level - geo_start);
            if((group_level - geo_start) > 1){
                $log.info("single group");
                keys = US.chain(qData)
                    .map(function(row){
                        return row.key.slice(geo_start, group_level);
                    })
                    .filter(function(row){return  US.contains(row, null)==false;})
                    .uniq(function(row){ return row.join("-")})
                    .value();
            }else{
                $log.info("big group");
                keys = US.chain(qData)
                    .map(function(row){return row.key.slice(geo_start, group_level);})
                    .filter(function(row){return  US.contains(row, null)==false;})
                    .flatten()
                    .uniq()
                    .value();
            }
            $log.info("keys", keys);
            var key_set = US.flatten(keys)
            var key_size = US.max(keys, function(key_pair){return key_pair.length}).length;
            //get unique key parts for label;
            $log.info("key set", key_set);
            var labels = US.chain(key_set)
                .countBy()
                .pairs()
                .filter(function(pair){if(pair[1]<2 && pair[0].length < 30){return true;}})
                .map(US.first)
                .value()
            $log.info("labels", labels);
            var data = {}
            data.datasets = {}
            //init labels
            labels.forEach(function(label){data.datasets[label]=[];});
            ;
            //bin data into temporal segments by geo-combination, organize and output
            $log.info("axis", axis);
            axis.forEach( function(x_set){
                var bin_result = null;
                if(x_set && Array === x_set.constructor){
                    if(x_set.length == 1){
                        bin_result = US.chain(qData)
                            .filter(function(row){
                                return US.contains(row.key, x_set[0]);
                            })
                            .value()
                    }else{
                        bin_result = US.chain(qData)
                            .filter(function(row){
                                //saves us many iterations by making sure all the keys are valid
                                //doesn't make sure all the keys are valid TOGETHER though.
                                //That happens when we push them into label groups
                                return US.intersection(row.key, x_set.concat(key_set)).length == x_set.length+key_size;
                            })
                            .value()
                    }
                }else{
                    bin_result = US.chain(qData)
                        .filter(function(row){
                            return parseInt(row.key[basic.axis_index],10) == x_set;
                        })
                        .value()
                }
                labels.forEach(function(label){
                    //$log.info(label);
                    var label_result = null;
                    label_result = US.chain(bin_result)
                        .filter(function(row){
                            //$log.info(row);
                            //$log.info(US.contains(row.key, label));
                            return US.contains(row.key, label);
                        })
                        .pluck("value")
                        .reduce(function(memo, vals){
                            US.each(US.keys(vals), function(key){
                                memo.hasOwnProperty(key) == true ? memo[key] += vals[key]: memo[key] = vals[key];
                            })
                            return memo;
                        })
                        .value()
                    $log.info(label, label_result);
                    if(label_result != null){
                        //TODO put this logic at the top
                        if(custom_value_transform != null){
                            data.datasets[label].push(custom_value_transform(label_result));
                        }else{
                            data.datasets[label].push(default_value_transform(label_result));
                        }
                    }else{
                        data.datasets[label].push(0);
                    }

                });
            });
            if(!basic.x_axis_is_temporal){
                data.labels = axis;
            }else{
                data.labels = axis;//US.map(axis, function(row){return row.join("-")});
            }

            data.meta = {"keys":keys,"names":labels,"level":current_level}
            return data;

        }

        var loadLevel = function(levelNumber){
            var url = formatUrl(levels[current_level]);
            $log.info(url);
            if (active_process < 1){
                var query = CouchService.load(url);
                active_process = active_process + 1;
                query.then(function(qData){
                    $log.info("qData", qData);
                    var out = xFormData(qData);
                    $rootScope.$broadcast("drill-data", out);
                    active_process = active_process - 1;
                });
            }else{
                $log.info("ignoring duplicate load level call");
            }
        }

        this.init = init;
        this.loadLevel = this.loadLevel;


    })
    .service('DVDataService', function($log, $q, CouchService, ColorService, US){

        this.test = function(clinicName){
            $log.info("Service Connected for " + clinicName);
            CouchService.clinicID(clinicName)
        }

        var formatDataSet = function(label, data, seriesNumber){
            var c = ColorService.getRGBASets(1, 4, seriesNumber, "0.6");
            var ret = {
                label: label,
                fillColor: c[0][0],
                strokeColor: c[0][0],
                pointColor: c[0][0],
                pointStrokeColor:   c[0][1],
                pointHighlightFill: c[0][0],
                pointHighlightStroke:  "#fff",
                data: data,
            }
            return ret;
        }

        this.queryDataSet = function(view){
            var defer = $q.defer();
            var dataset = {};
            var apiCall = view.url+view.args;
            var query = CouchService.load(apiCall);
            query.then(function(qData){
                if(view.simpleValues){
                    var key = view.valueKeys;
                    dataset.size = 1;
                    dataset.keys = [key];
                    dataset.xaxis = [];
                    var data = {};
                    data[key] = {};
                    for(var c in qData){
                        var keyParts = qData[c].key;
                        var xLabel = [];
                        for(var i in view.xaxisparts){
                            var idx = view.xaxisparts[i];
                            xLabel.push(keyParts[idx]);
                        }
                        dataset.xaxis.push(xLabel.join("-"));
                        var row = qData[c].value;
                        var val = (row != null? row :0);
                        data[key][xLabel.join("-")] = val;
                    }
                    dataset.rawdata = data;
                    defer.resolve(dataset);
                }else{
                    dataset.size = view.valueKeys.length;
                    dataset.keys = view.valueKeys;
                    dataset.xaxis = [];
                    var data = {};
                    //init output arrays
                    for(var x in view.valueKeys){
                        var key = view.valueKeys[x];
                        data[key] = {};
                    }
                    //grab values from rows and organize
                    for(var c in qData){
                        var keyParts = qData[c].key;
                        var xLabel = [];
                        for(var i in view.xaxisparts){
                            var idx = view.xaxisparts[i];
                            xLabel.push(keyParts[idx]);
                        }
                        dataset.xaxis.push(xLabel.join("-"));
                        var row = qData[c].value;
                        for(var x in dataset.keys){
                            var key = dataset.keys[x];
                            var val = (row[key] != null? row[key] :0);
                            data[key][xLabel.join("-")] = val;
                        }
                    }
                    dataset.rawdata = data;
                    $log.info(dataset);
                    defer.resolve(dataset);
                }
            });
            return defer.promise;
        }

        this.setData  = function(input){
            var data = {};
            data.labels = input.labels;
            data.datasets = [];
            var seriesNumber = 10;
            for( var label in input.datasets) {

                data.datasets.push(
                    formatDataSet(
                        label,
                        input.datasets[label],
                        seriesNumber
                    ));
                seriesNumber+=1;
            };
            $log.info(data);
            return data;
        }

        this.setDataAffinity = function(data, affinity){
            $log.info(data);
            //clone the set so we don't end up modifying things in-place by reference
            var outData = US.clone(data);
            //preps a list for us to grab element by key and affinity
            var affinity_sets = US.chain(data.datasets)
                .map(function(val, key){
                    return US.map(affinity, function(af){
                        return {"name": String(key + " - " + af), "key":key, "affinity":af};
                    });
                })
                .flatten(true)
                .value()
            $log.info("affinity sets: ", affinity_sets);
            var new_datasets = US.chain(affinity_sets)
                .map(function(r){
                    var out = US.map(data.datasets[r.key], function(i){
                        return i[r["affinity"]] != null ? i[r["affinity"]] : 0;
                    });
                    return [r.name, out];
                })
                .object()
                .value()
            $log.info(new_datasets);
            outData.datasets = new_datasets;
            /*data.meta.names = US.chain(affinity_sets)
                .pluck("name")
                .value();
            */
            return outData;


        }

        this.getData = function(views){
            var returnedData = 0;
            var datasets = [];
            var labels = [];
            var defer = $q.defer();
            var seriesNumber = 0;
            var viewCount = views.length;
            for(var idx in views){
                var view = views[idx];
                var ds = this.queryDataSet(view);
                ds.then(function(dataset){
                    datasets.push(dataset)
                    returnedData +=1;
                    if(returnedData >= viewCount){
                        $log.info("got all views: " + returnedData)
                        //Assemble output
                        var uniqueLabels = {};
                        datasets.forEach(function(dataset){
                            dataset.xaxis.forEach(function(label){
                                if (!(label in uniqueLabels)){
                                    uniqueLabels[label] = true
                                }
                            });
                        });
                        uniqueLabels = Object.keys(uniqueLabels).sort();
                        $log.info(uniqueLabels);
                        var data = {};
                        data.labels = uniqueLabels;
                        data.datasets = [];
                        var seriesNumber = 0;
                        datasets.forEach(function(dataset){
                            Object.keys(dataset.rawdata).forEach(function(setName){
                                var rawdata = dataset.rawdata[setName];
                                var outdata = [];
                                uniqueLabels.forEach(function(label){
                                    var val = rawdata[label];
                                    val != null ? outdata.push(val) : outdata.push(0);
                                });
                                $log.info("new set: " + setName)
                                data.datasets.push(
                                    formatDataSet(
                                        setName,
                                        outdata,
                                        seriesNumber
                                ));
                                seriesNumber+=1;
                            });
                        });
                        defer.resolve(data)
                    }else{
                        $log.info("still need: " + (viewCount - returnedData))
                    }
                });
            }
            return defer.promise;
        }

    })
    .directive('barChart', function($log, DVDataService, Chart, CouchService, ColorService, US, $rootScope) {
        return {
            templateUrl: '/static/app/dataviz/barChart.html',
            replace: true,
            restrict: 'E',
            scope: {
                chartType: '@charttype',
                name: '@name',
                clinic:'@clinic',
                clinicID: '@clinicid',
                legend: '@legend',
                header: '@headers',
                view : '@views',
                height: '@height',
                width: '@width',
                fill: '@fill',
                affinity:"@affinity"
            },
            link: function(scope, elems, attrs){

                $log.info('caught link from barChart');
                var ctx = elems.find("canvas")[0].getContext("2d");
                var loadViews = function(){
                    $log.info("chart has views set in template.")
                    var views = [];
                    if(scope.header != null){
                        scope.hasHeaders = true;
                        scope.headers = JSON.parse(scope.header);
                        $log.info(scope.headers);
                    }else{
                        scope.hasHeaders = false;
                        $log.info('no header provided');
                    }
                    var loadViews = JSON.parse(scope.view);
                    loadViews.forEach(function(view){
                        var out = view;
                        out.args = view.args.join('"');
                        views.push(view);
                    });
                    $log.info(views);
                    return views;

                }
                var datasets = {};
                var loadData = function(views){
                    var dataQ = DVDataService.getData(views);
                    dataQ.then(function(data){
                        $log.info('Finished getting data', data);
                        showChart(data);
                    });
                }

                var showChart = function(data){
                    $log.info("showing chart!");
                    if(attrs.fill != null){
                        $log.info("fill", attrs.fill);
                        scope.fill = JSON.parse(attrs.fill);
                        if (!scope.fill){
                            chartArgs["datasetFill"] = false;
                            chartArgs["datasetStrokeWidth"] = 7 ;
                        }
                    }
                    if(ch != null){ch.clear();ch.destroy();}
                    switch(scope.chartType){
                        case "bar":
                            ch = new Chart(ctx).Bar(data, chartArgs);
                            chartClick = function(evt){return ch.getBarsAtEvent(evt);}
                            clickBroadcaster = function(evt, evtInfo){
                                var clickX = evt.layerX, clickY = evt.layerY;
                                var nearest = US.chain(evtInfo)
                                    .min(function(pt){
                                        return Math.abs(clickX - pt.x);
                                    })
                                    .value();
                                $rootScope.$broadcast('chart-click', nearest);
                            }
                            break
                        case "line":
                            ch = new Chart(ctx).Line(data, chartArgs);
                            chartClick = function(evt){return ch.getPointsAtEvent(evt);}
                            clickBroadcaster = function(evt, evtInfo){
                                    var clickX = evt.layerX, clickY = evt.layerY;
                                    var nearest = US.chain(evtInfo)
                                        .min(function(pt){
                                            return(((clickX - pt.x) *(clickX - pt.x)) + ((clickY - pt.y) *(clickY - pt.y)));
                                        })
                                        .value();
                                    $rootScope.$broadcast('chart-click', nearest);
                                }
                            break
                        case "radar":
                            ch = new Chart(ctx).Radar(data, chartArgs);
                            break
                        default:
                            ch = new Chart(ctx).Bar(data, chartArgs);
                            break
                    }
                    var data = {}
                    var legend = ch.generateLegend();
                    data['legend'] = legend
                    data['name'] = scope.legend;
                    $log.info('generated legend', data);
                    $rootScope.$broadcast('set-legend', data);
                }

                var chartClick = null;
                var clickBroadcaster = function(evt, evtInfo){return null;};

                var chartArgs = {
                    pointDotRadius: 5,
                    bezierCurve: false,
                    scaleShowVerticalLines: true,
                    scaleGridLineColor: 'gray',
                    labelFontFamily : "Arial",
                    labelFontStyle : "normal",
                    labelFontSize : 24,
                    labelFontColor : "#666",
                    maintainAspectRatio:false,
                    multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>",
                    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
                    responsive:true
                }

                var ch = null;
                var getChartElement = null;

                var initChart = function(){
                    if(scope.view != null){
                        var views = loadViews();
                        loadData(views);
                    }
                    else{
                        $log.info("chart is dynamic or missing data");
                    }
                }

                //looks for new data being injected into the html of the parent template
                scope.$watch('view', function(newVal, oldVal){
                    $log.info('watch triggered on chart.scope.view');
                    if(newVal != oldVal){
                        $log.info(newVal);
                        if(ch != null){ch.clear();ch.destroy();}
                        initChart();
                    }
                });

                initChart();

                elems.bind('click', function(evt){
                    $log.info("click on chart...");
                    if (chartClick != null){
                        var evtInfo = chartClick(evt);
                        if(evtInfo.length > 0){
                            clickBroadcaster(evt, evtInfo);
                        }else{
                            $log.info("nothing at point")
                        }

                    }
                });
                scope.$on('drill-data', function(event, data){
                    $log.info("chart got data from drill", data);
                    var out = null
                    if(attrs.affinity != null){
                        scope.affinity = JSON.parse(attrs.affinity);
                        $log.info("chart with affinity got data from drill:", scope.affinity, data);
                        out = DVDataService.setDataAffinity(data, scope.affinity)
                        out = DVDataService.setData(out);
                    }else{
                        out = DVDataService.setData(data);
                    }
                    showChart(out);
                });
            },
        };
    }).directive('tableView', function($log, DVDataService, Chart, CouchService, ColorService, CSVService) {
        return {
            templateUrl: '/static/app/dataviz/tableView.html',
            replace: true,
            restrict: 'E',
            scope: {
                clinic:'@clinic',
                clinicID: '@clinicid',
                header: '@headers',
                view : '@views',
                name: '@name',
                affinity: '@affinity',
                source: '@source'
            },
            link: function(scope, elems, attrs){

                $log.info('caught link from tableView');
                $log.info(elems);

                scope.selectedIndex = null;
                scope.selectedLabel = null;

                scope.getClass = function(row, index){
                    var label = row.label;
                    if(scope.selectedIndex == index && scope.selectedLabel === label){
                        return 'selected-item';
                    }
                    if (scope.selectedLabel === label && index == null){return 'selected-header';}
                    if (index == scope.selectedIndex && index != null){return 'selected-column';}
                    return '';
                }

                var loadViews = function(){
                    var views = [];
                    if(scope.header != null){
                        scope.hasHeaders = true;
                        scope.headers = JSON.parse(scope.header);
                        $log.info(scope.headers);
                    }else{
                        scope.hasHeaders = false;
                        $log.info('no header provided');
                    }
                    var loadViews = JSON.parse(scope.view);
                    loadViews.forEach(function(view){
                        var out = view;
                        out.args = view.args.join('"');
                        views.push(view);
                    });
                    $log.info(views);
                    return views;

                }
                var loadData = function(views){
                    var dataQ = DVDataService.getData(views);
                    dataQ.then(function(data){
                        $log.info('Finished getting data', data);
                        scope.data = data;
                    });
                }
                var initTable = function(){
                    var views = loadViews();
                    loadData(views);
                }

                if(scope.view != null){
                    initTable();
                }

                //looks for new data being injected into the html of the parent template
                scope.$watch('view', function(newVal, oldVal){
                    $log.info('watch triggered on table.scope.view');
                    if(newVal != oldVal){
                        $log.info(newVal);
                        initTable();
                    }
                });

                elems.bind('click', function(e){
                    $log.info("click on table...");
                });

                var handleNewData = function(event, data){
                    $log.info("new chart data", data);
                    scope.selectedIndex = null;
                    scope.selectedLabel = null;
                    scope.data = null;
                    if(attrs.affinity != null){
                        var out = null
                        scope.affinity = JSON.parse(attrs.affinity);
                        $log.info("table with affinity got data from drill:", scope.affinity, data);
                        out = DVDataService.setDataAffinity(data, scope.affinity)
                        scope.data = DVDataService.setData(out);
                    }else{
                        //$log.info("table has no affinity");
                        scope.data = DVDataService.setData(data);
                    }

                }

                scope.$on('set-table-data', function(event, data){
                    //$log.info("table got data from set-table-data", data);
                    if(attrs.source != null){
                        scope.source = attrs.source;
                        if(scope.source != data.source){
                            $log.info("ignoring data from source: " + data.source);
                            return;
                        }else{
                            $log.info("loading data from source: " + data.source);
                            handleNewData(event, data);
                        }

                    }
                });

                scope.$on('drill-data', function(event, data){
                    //$log.info("table got data from drill", data);
                    handleNewData(event, data);
                });

                scope.$on('chart-click', function(event, data){
                    $log.info("table got data from chart")
                    scope.selectedIndex = scope.data.labels.indexOf(data.label);
                    scope.selectedLabel = data.datasetLabel;
                    scope.$digest()
                });

                scope.getCSV = function(){
                    $log.info("table got csv request")
                    $log.info(scope.data);
                    var name = "table_values";
                    if (scope.name != null){
                        name = scope.name;
                    }
                    name = name + '-' + new Date().toISOString();
                    $log.info(name);
                    CSVService.getCSV(scope.data, name);
                };

            },
        };
    }).directive('chartLegend', function($log, $rootScope, $sce) {
            return {
                templateUrl: '/static/app/dataviz/chart-legend.html',
                replace: true,
                restrict: 'E',
                scope: {
                    name: '@name'
                },
                link: {
                    pre: function(scope, elems, attrs){

                        scope.rows = ['a','b','c'];
                        $log.info('initial chart-legend scope: ', scope.rows);
                        scope.$on('set-legend', function(evt, data){
                            $log.info('legend caught event!' , data);
                            if (scope.name == data.name){
                                scope.rows = $sce.trustAsHtml(data.legend);
                            }
                        });

                    }
                }
            }
    }).directive('drillDisplay', function($log, DrillService, US, translateFilter) {
        return {
            templateUrl: '/static/app/dataviz/drillController.html',
            replace: true,
            restrict: 'E',
            scope: {
                levels : '@data'
            },
            link: function(scope, elems, attrs){
                scope.items = [];
                scope.level = 0;
                scope.loaded = false;
                $log.info('caught link from drillController');

                scope.$on('date-set', function(evt,data){
                    $log.info('drill caught new date');
                    if(scope.levels != null){
                        if (scope.loaded == false){
                            scope.levels = JSON.parse(scope.levels);
                            scope.loaded = true;
                        }
                        scope.levels.data.start = data.startParts.slice(0,2).join("-");
                        scope.levels.data.end = data.endParts.slice(0,2).join("-");

                        $log.info("found levels for drill");
                        $log.info(scope.levels);
                        DrillService.init(scope.levels, scope.level);
                    }
                });

                scope.$on('drill-data', function(event, data){
                    $log.info('notify drill caught')
                    var items = US.object(data.meta.names, data.meta.keys)
                    scope.level = data.meta.level;
                    scope.items = US.map(items, function(v, k){
                        return {"name": k, "value": v};
                    });
                });

            }
        };
    }).directive('monthPicker', function($rootScope, $log){
        return {
            templateUrl: '/static/app/dataviz/monthPicker.html',
            restrict: 'E',
            replace: true,
            scope: {},
            link: {
                pre: function(scope, elems, attrs){
                    scope.processDate = function(date){
                        scope.month = date.getMonth()+1;
                        scope.year = date.getFullYear();
                        scope.date_string = date.toISOString().slice(0,7);
                    }
                    scope.rawDate = null;
                    scope.init = new Date();
                    scope.init.setMonth(scope.init.getMonth() - 2);
                    scope.processDate(scope.init);
                },

                post: function(scope, elems, attrs){
                    $rootScope.$on('request-month', function(evt, data){
                        $log.info('request-month', data);
                        if (data != null){
                            if (data['request-type'] != null){
                                if (data['request-type'] == 'change-date'){
                                    var change_request = data['change-request']
                                    var field = change_request['field'];
                                    var increment = change_request['increment']
                                    if (field == 'month'){
                                        if (scope.month >= 12 && increment > 0){
                                            scope.year +=1;
                                            scope.month = 1;
                                        }
                                        else if (scope.month <= 1 && increment < 0){
                                            scope.year -=1;
                                            scope.month = 12;
                                        }else{
                                            scope.month += increment;
                                        }

                                    }
                                }
                            }
                        }
                        scope.send();
                    });

                    scope.send = function(){
                        $rootScope.$broadcast('month-set', {"start":{'year': scope.year, 'month': scope.month, 'day': 1}, "end":{'year': scope.year, 'month': scope.month, 'day': 32}});
                    }

                    scope.setDate = function(selection){
                        if(selection != null){
                            var input = new Date(Date.parse(selection.date));
                            scope.processDate(input);
                        }
                        scope.send();
                    }
                    scope.send();
                }
            }

        };
    }).directive('dateRange', function($log, $rootScope, translateFilter) {
        return {
            templateUrl: '/static/app/dataviz/dateRange.html',
            replace: true,
            restrict: 'E',
            scope: {
                window : '@window',
                getStartString: '@',
                getEndString: '@'
            },
            link: {
                pre: function(scope, elems, attrs){
                    if (scope.window == null){
                        scope.window = 6;
                    }

                    scope.start = null;
                    scope.end = null;

                    scope.getPartsFromDate = function(date){
                        $log.info(date);
                        return date.toISOString().slice(0,10).split('-');
                    }

                    var getStringFromParts = function(parts){
                        return parts.join("-");
                    }

                    scope.getStart = function(){
                        $log.info("start val");
                        $log.info(scope.start);
                        if (scope.start != null){
                            return scope.start;
                        }else{
                            var start = new Date(scope.getEnd().getTime());
                            $log.info(start);
                            $log.info(start.getMonth());
                            start.setDate(1);
                            start.setMonth(start.getMonth()-scope.window);
                            $log.info(start);
                            scope.start = start;
                        }
                        return scope.start;
                    }
                    scope.getEnd = function(){
                        if (scope.end != null){
                            return scope.end;
                        }else{
                            scope.end = new Date();
                        }
                        $log.info('end');
                        $log.info(scope.end);
                        return scope.end;
                    }

                    scope.getStartString = function(){
                        return scope.getPartsFromDate(scope.getStart()).join("-");
                    }
                    scope.getEndString = function(){
                        return scope.getPartsFromDate(scope.getEnd()).join("-");
                    }


                    scope.startString = scope.getStartString();
                    scope.endString = scope.getEndString();

                },
                post:function(scope, elems, attrs){
                    $log.info('POST-link');

                    var broadcast = function(){
                        var data = {"start":scope.start, "end": scope.end};
                        data['startParts'] = scope.getPartsFromDate(scope.start);
                        data['endParts'] = scope.getPartsFromDate(scope.end);
                        $log.info('date-set');
                        $log.info(data);
                        $rootScope.$broadcast("date-set", data);
                    }


                    scope.$on('request-date', function(evt, data){
                        $log.info('date picked caught request');
                        broadcast();
                    });

                    scope.selectDate = function(input){
                        $log.info('selected date:');
                        $log.info(input);
                        if (input.startDate != null){
                            scope.start = new Date(Date.parse(input.startDate));
                        }
                        if(input.endDate != null){
                            scope.end = new Date(Date.parse(input.endDate));
                        }
                        broadcast();
                    }
                    broadcast();
                }
            }
        };
    }).controller('DrillController', function($scope, $log, $rootScope){
        var data = null;
        $scope.level = 0;

        var working = false;

        this.working = function(){
            return working;
        }
        var final = false;

        this.init = function(input){
            data = input;
            $scope.level = $scope.$parent.level;
            final = Object.keys($scope.$parent.levels).length+1 == $scope.level;
        }

        this.isZero = function(){
            return $scope.level == 0;
        }

        this.style = function(){
            if(working || final){
                return {'color':'grey'};
            }else{
                return null;
            }

        }

        $rootScope.$on("date-set", function(evt, data){
            working = true;
        });


        $rootScope.$on("notify-drill", function(evt, data){
            working = true;
        });

        this.select = function(){
            if(final){
                return;
            }
            if(!working){
                $rootScope.$broadcast("notify-drill", data);
            }
        };
        $scope.$on('drill-data', function(event, evtdata){
            working = false;
            $log.info("meta", evtdata.meta)
            $scope.level = evtdata.meta.level;
        });
    })

