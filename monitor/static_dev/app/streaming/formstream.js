'use strict';

angular.module('Monitor.Streaming.FormStream', ['MonitorServices.CouchService', 'ClinicMap.Base','Monitor.Translate'])
    .factory('US', function() {
        return window.underscore; // assumes underscore has already been loaded on the page
    })
    .factory('moment', function() {
        return window.moment; // assumes moment has already been loaded on the page
    })
    .service('FormMonitor', function(CouchService, $log, $rootScope, $q, $interval, US){
        var running = false;
        var update_time = null;
        var last = null;
        var db = null;

        var poll = function(db){
            var deferred = $q.defer();
            var req = getLastChange(db);
            req.then(function(lastChange){
                $log.info(lastChange)
                if (last == null){
                    last = lastChange - 20;
                    deferred.resolve(true);
                }else if ( last != lastChange) {
                    deferred.resolve(true);
                }else{
                    deferred.resolve(false);
                }
            });
            return deferred.promise;
        }

        var update = function(){
            update_time = new Date();
            $log.info("updating:", db, update_time);
            var req_poll = poll(db);
            req_poll.then(function(has_new_data){
                if(has_new_data){
                    $log.info("new data available");
                    var old_seq = last;
                    var req_changes = CouchService.changes(db, old_seq, true);
                    req_changes.then( function(response){
                        last = response.last_seq;
                        US.each(response.results, function(change){
                            //$log.info(change);
                            $rootScope.$broadcast("form-stream-event", change)
                        });
                    });
                }
                else{
                    $log.info("no new data");
                }
            });
        }

        this.start = function(db_name){
            if (running==true) {
                $log.info("service already running");
                return;
            }
            running = true;
            db = db_name;
            update();
            $interval(update, 30000);
            return;
        }

        var getLastChange = function(db){
            var req = CouchService.lastChange(db);
            return req;
        }
    })
    .directive('formTicker', function($log, $rootScope ,moment ,FormMonitor, translateFilter) {
        return {
            templateUrl: '/static/app/streaming/ticker.html',
            replace: true,
            restrict: 'E',
            scope: {
                name: '@name'
            },
            link: function(scope, elems, attrs){

                function convertTZ(update_time){
                    return moment.tz(update_time, "GMT").local().format("HH:MM:SS");
                }

                scope.flash = function(_id){
                    $log.info("flash:", _id);
                    $rootScope.$broadcast("form-stream-map-flash", _id);
                }

                scope.events = [];
                FormMonitor.start("form")
                $rootScope.$on("form-stream-event", function(evt, data){
                    //$log.info('ticker caught', data);
                    var packet = {
                        "received": convertTZ(data.doc.received_on),
                        "user": data.doc.metadata.username,
                        "user_id": data.doc.metadata.userID
                        };
                    scope.events.push(packet);
                    scope.events = scope.events.slice(-20);

                });
            }
        };

    }).controller('FormStreamMap', function($scope, $log, $rootScope, $timeout, MapService) {

        //icons for icon intensity
        var getIconUrl = function(level){
            var p = "/static/mapicons/"
            var urls = {
                0: p+"firedept.png",
                1: p+"red.png",
                2: p+"orange.png",
                3: p+"yellow.png",
                4: p+"green.png",
                5: p+"grey.png",
            }
            if (urls[level] != null){
                return urls[level];
            }else{
                return urls[5];
            }
        }

        function color(marker){
            marker[0].setIcon({
                url: getIconUrl(5),
                labelOrigin: new google.maps.Point(15.5, 10),
            });
        }

        $scope.flashDuration = function(map, markers, _id, duration){
            var marker = markers[_id][0];
            marker.setIcon({
                url: getIconUrl(4),
                labelOrigin: new google.maps.Point(15.5, 10)
            });
            try{
                if (marker.getAnimation() != null){
                    throw new Error('already bouncing');
                }
                marker.setAnimation(google.maps.Animation.BOUNCE);
                $timeout(function(){
                    marker.setAnimation(null);
                    marker.setIcon({
                        url: getIconUrl(5),
                        labelOrigin: new google.maps.Point(15.5, 10)
                    });
                }, duration*1000);
            }catch (e){
            }


        }

        $scope.flash = function(map, markers, datum){
            var _id = datum.doc.metadata.userID
            try{
            $scope.flashDuration(map, markers, _id, 30);
            }catch (e){}
        }

        $scope.init = function(elem){
            $log.info("formstreammap caught init");

            MapService.init(elem, null, color, null);
            var map = MapService.getMap();
            var markers = MapService.getMarkers();
            $scope.$on("form-stream-event", function(evt,datum){
                $scope.flash(map, markers, datum)
            });
            $scope.$on("form-stream-map-flash", function(evt, _id){
                $scope.flashDuration(map, markers, _id, 3);
            });
        }


    });


