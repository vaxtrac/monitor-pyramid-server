'use strict';

angular.module('ClinicMap.NetworkMap', ['MonitorServices.CouchService', 'MonitorWidgets.Widgets', 'ClinicMap.Base'])
    .factory('US', function() {
        return window.underscore; // assumes moment has already been loaded on the page
    })
    .factory('google', function() {
        return window.google; // assumes moment has already been loaded on the page
    })
    .factory('hull', function(){
        return window.hull;
    })
    .service('NetworkMapService', function(MapService, CouchService, $log, $rootScope, $q, google, US, hull){

        var clinic_loc = {};
        var clinic_traffic = {};
        var traffic = {};
        var groups = {};
        var lost_clinics = {};

        var makePolygons = function(group_level){

            //poly example: https://developers.google.com/maps/documentation/javascript/examples/polygon-arrays
            var markers = MapService.getMarkerArray();

            var unique_keys = US(markers).chain()
                .map( function(obj){
                    if (obj[group_level] != null){ return obj[group_level]}
                })
                .unique()
                .value();

            clinic_loc = US(markers).chain()
                .map(function(obj){
                    return [obj['clinicID'],{'lat':obj['lat'], 'lng':obj['lng']}];
                })
                .object()
                .value()

            unique_keys.forEach(function(key){
                //make polygons for each clinic group using convex hull as border
                groups[key] = {}
                groups[key]["pos"] = US(markers).chain()
                    .filter(function(row){
                        return row[group_level] == key;
                    })
                    .map(function(obj){
                        return {"lat": obj.lat, "lng":obj.lng}
                    })
                    .value()
                groups[key]['hull'] = hull(groups[key]['pos'], 20, ['.lng', '.lat']);
                groups[key]['poly'] = new google.maps.Polygon({
                    paths: groups[key]['hull'],
                    strokeColor: '#000000',
                    strokeOpacity: 0.2,
                    strokeWeight: 1,
                    fillColor: '#FF0000',
                    fillOpacity: 0.10
                });

            });
            return groups;
        }

        this.get_traffic = function(){

            var def = $q.defer();
            var url = '/api/form/network/form-submit/json?filter=true&reduce=true&group_level=1&exclude_null=true&cache=true'
            var query = CouchService.load(url);
            query.then(function(res){
                var lines = {}
                res.forEach( function(action){
                    var unique_combinations = US(action.value).chain()
                        .keys()
                        .map( function(act1){
                            var out = []
                            US.each(Object.keys(action.value), function(act2){
                                if (act1 != act2){
                                    act1 < act2 ? out.push(act1+"-"+act2) : out.push(act2+"-"+act1);
                                }
                            });
                            return out;
                        })
                        .flatten()
                        .unique()
                        .value()
                    unique_combinations.forEach(function(pair){
                        var c1 = pair.split('-')[0];
                        var c2 = pair.split('-')[1];


                        if (pair in lines){
                            clinic_traffic[c1] != null ? clinic_traffic[c1] +=1 : clinic_traffic[c1] =1;
                            clinic_traffic[c2] != null ? clinic_traffic[c2] +=1 : clinic_traffic[c2] =1;
                            lines[pair]['count'] += 1;
                        }else{
                            if (clinic_loc[c1] != null && clinic_loc[c2] != null){
                                //$log.info('valid pair', pair)
                                clinic_traffic[c1] != null ? clinic_traffic[c1] +=1 : clinic_traffic[c1] =1;
                                clinic_traffic[c2] != null ? clinic_traffic[c2] +=1 : clinic_traffic[c2] =1;
                                lines[pair] = {};
                                lines[pair]['c1'] = c1;
                                lines[pair]['c2'] = c2;
                                lines[pair]['l1'] = clinic_loc[c1];
                                lines[pair]['l2'] = clinic_loc[c2];
                                lines[pair]['count'] =1 ;
                            }else{
                                if (clinic_loc[c1] == null){
                                    lost_clinics[c1] != null ? lost_clinics[c1] +=1 : lost_clinics[c1] = 1;
                                }
                                if (clinic_loc[c2] == null){
                                    lost_clinics[c2] != null ? lost_clinics[c2] +=1 : lost_clinics[c2] = 1;
                                }
                                //$log.info('BAD pair', pair)
                            }
                        }

                    });

                });
                $log.info("lost clinics", lost_clinics);
                def.resolve(lines);
            });
            return def.promise;
        }

        this.getClinicTraffic = function(clinicID){
            try{
                return clinic_traffic[clinicID];
            }catch(err){
                $log.info('no traffic for clinic', clinicID, err);
                return 0;
            }
        }

        this.makePolygons = makePolygons;


    }).controller('NetworkMap', function($scope, $log, $rootScope, $compile, $timeout, MapService, NetworkMapService) {


        var map = null;
        var infoWindow = null;

        function color(marker){
            marker[0].setIcon({
                url: '/static/mapicons/green_dot.png',
                labelOrigin: new google.maps.Point(0,0),
            });
            marker[0].setLabel(null);
        }

        function draw_lines(lines){
            $log.info(lines);
            for (var key in lines){
                var ln = lines[key]
                var connection =new google.maps.Polyline({
                        path: [ln['l1'], ln['l2']],
                        geodesic: true,
                        strokeColor: '#FF0000',
                        strokeOpacity: .5,
                        strokeWeight: ln['count']
                    });
                connection.setMap(map);

                function view(l){
                    var args = '<feature-desc c1="'+l['c1']+'" c2="'+l['c2']+'" count="'+l['count']+'"></feature-desc>';
                    var content = $compile(args)($scope);
                    $scope.$apply();
                    infoWindow.setContent(content[0].innerHTML);
                    $scope.$watch(function()
                            {
                                return content[0].innerHTML
                            },
                        function(newValue, oldValue) {
                            infoWindow.setContent(newValue);
                    });
                    var middle = {};
                    middle.lat = (l.l1.lat + l.l2.lat)/2
                    middle.lng = (l.l1.lng + l.l2.lng)/2
                    infoWindow.setPosition(middle);
                    infoWindow.open(map);

                }
                connection.addListener('click', view.bind(this, ln));
            }
            infoWindow = new google.maps.InfoWindow;
        }

        $scope.init = function(elem){
            $log.info("formstreammap caught init");

            MapService.init(elem, null, color, null);
            map = MapService.getMap();
            var markers = MapService.getMarkers();

            $scope.$on("form-stream-event", function(evt,datum){
                $scope.flash(map, markers, datum)
            });
            $scope.$on("form-stream-map-flash", function(evt, _id){
                $scope.flashDuration(map, markers, _id, 3);
            });
            $scope.$on('map-loaded', function(evt, data){
                var groups = NetworkMapService.makePolygons("group");
                map = MapService.getMap();
                for (var key in groups){
                    groups[key]['poly'].setMap(map);
                }
                var pro = NetworkMapService.get_traffic();
                pro.then(function(lines){
                    draw_lines(lines);
                });
            });
        }


    }).directive('featureDesc', function($log, $rootScope, NetworkMapService, CouchService) {
        return {
            templateUrl: '/static/app/clinicmap/networkFeatureDesc.html',
            replace: true,
            restrict: 'E',
            scope: {
                c1: '@c1',
                c2: '@c2',
                count: '@count'
            },
            link: function(scope, elems, attrs){

                scope.c1_name = null;
                scope.c2_name = null;
                scope.c1_reg = 1.0;
                scope.c2_reg = 1.0;

                function getName(clinicID, watcher){
                    var url = '/api/lookup/python-group/clinicid-info/json?filter=false&reduce=false&key="'+clinicID+'"&limit=1';
                    var query = CouchService.load(url);
                    query.then(function(res){
                        var out = angular.fromJson(res);
                        scope[watcher] = out[0]['value']['clinic_name'];
                    });
                }

                scope.getTraffic = function(clinicID){
                    return NetworkMapService.getClinicTraffic(clinicID);
                }

                function getRegistration(clinicID, watcher){
                    var url = '/api/form/py-form-type/clinic_id-form-types/json?reduce=true&group_level=1&key="'+clinicID+'"';
                    var query = CouchService.load(url);
                    query.then(function(res){
                        var out = angular.fromJson(res);
                        scope[watcher] = out[0]['value']['registration'];
                    });
                }


                function init(){
                    $log.info(scope);
                    getName(scope.c1, 'c1_name');
                    getName(scope.c2, 'c2_name');
                    getRegistration(scope.c1, 'c1_reg');
                    getRegistration(scope.c2, 'c2_reg');
                }

                init();


            }
        }
    });


