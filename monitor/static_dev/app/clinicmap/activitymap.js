'use strict';

angular.module('ClinicMap.Activity', ['MonitorServices.CouchService', 'MonitorWidgets.Widgets', 'ClinicMap.Base'])
    .service('ActivityMapService', function(MapService, CouchService, $log, $rootScope){

        /*
            Functions from ClinicMap.Base set on init
        */

        var getMap;
        var getInfoWindow;
        var colorMarker;
        var getIconUrl;

        /*
            utility functions
        */

        var dateFilter = function(d){
            return d[1]+"-"+d[2]+"-"+d[3];
        };

        function parseDate(str) {
            var dd = str.split('-')
            return new Date(dd);
        }

        function daysOld(date){
            //$log.info(date)
            return Math.round((new Date()-date)/(1000*60*60*24));
        }

        /*
            show / color related functions
        */

        var getContent = function(i){
            return '<div id="infobox-text"><p><b><u>'+i.longlabel+'</u></b></p>Last Submission: '+i.lastSeen+'<br><a href="/clinic/'+i.longlabel.toLowerCase()+'/status">View '+i.longlabel+'</a></div>';
        }

        function legend(){
            var data = {};
            data['headers'] = ["Days since submission", "Marker"];
            data['body'] = [[" < 7", getIconUrl(4)],
                            [" < 14", getIconUrl(3)],
                            [" > 14", getIconUrl(1)],
                           ];
            return data;
        }

        function color(marker){
            //$log.info(marker);
            var pro = getLastUpdate(marker[1].clinicID);
            pro.then(function(data){
                var ls = dateFilter(data[0].key);
                var since = daysOld(parseDate(ls));

                //$log.info("return for :" +marker[1].label + " | " + ls + " | " + since );
                if (since <= 7){
                    colorMarker(marker[1].clinicID, 4);
                }else if (since < 14){
                    colorMarker(marker[1].clinicID, 3);
                }else{
                    colorMarker(marker[1].clinicID, 1);
                }
            });
        }


        var getLastUpdate = function(clinicID){
            var req = '/api/form/activity/byclinicanddate/json?reduce=true&group_level=5&startkey=["'+clinicID+'",{}]&endkey=["'+clinicID+'"]&descending=true&limit=1'
            //returns Promise, not value
            return CouchService.load(req)
        }

        var show = function(marker){
            var cb = getLastUpdate(marker.clinicID);
            cb.then( function(data){
                //on api return populate the data and show the window
                marker.lastSeen = dateFilter(data[0].key);
                var infoWindow = getInfoWindow();
                var map = getMap();
                infoWindow.setContent(getContent(marker));
                infoWindow.open(map,marker);
            });
        }

        /*
            init & bind to ClinicMap.Base
        */


        this.init = function(elem) {
            getMap = MapService.getMap;
            getInfoWindow = MapService.getInfoWindow;
            colorMarker = MapService.colorMarker;
            getIconUrl = MapService.getIconUrl;
            MapService.init(elem, show, color, legend);

        }


    }).controller('ActivityMapController', function($scope, $compile, $filter, $log, ActivityMapService){
        $scope.init = function(elem){
            ActivityMapService.init(elem);
            return;
        }
    });
