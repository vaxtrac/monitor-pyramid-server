'use strict';

angular.module('ClinicMap.Base', ['MonitorServices.CouchService', 'MonitorWidgets.Widgets'])
    .factory('US', function() {
        return window.underscore; // assumes underscore has already been loaded on the page
    })
    .filter('object2Array', function(US) {
        return function(input) {
            return US.toArray(input);
        }
    })
    .filter('latlongContentFilter', function(){
        return function(row){
            try{
                var label = "";
                try{
                    var numerals = row.key[4].replace( /^\D+/g, '');
                    label = row.key[4].split("_").map(
                            function(i){
                                if (i.length > 3){
                                return i[0]
                                }
                                return i;
                            }
                        )
                    if (numerals.length > 0){
                        label.push(numerals);
                    }
                    label = label.join("-");

                }catch (err2){
                    label = row.key[4];
                }
                return {'dds': row.key[0],'zone': row.key[1],'group': row.key[2],'clinicID':row.key[3],'label': label, "longlabel": row.key[4], 'lat': parseFloat(row.value.lat, 10), 'lng': parseFloat(row.value.long, 10)}

            } catch (err){
                return {'dds': row.key[0],'zone': row.key[1],'group': row.key[2],'clinicID':row.key[3],'label': row.key[4], "longlabel": row.key[4], 'lat': parseFloat(row.value.lat, 10), 'lng': parseFloat(row.value.long, 10)}
            }
        }
    })
    .filter('groupFilter', function(){
        return function(group, memberName){
            if(memberName.slice(0,1)== group){
                return true;
            }
            return false;
        }
    })
    .service('MapService', function(CouchService, $log, $rootScope, $filter, $window){

        var map = null;
        var data = null;
        var markers = {};
        var marker_array = [];
        var infoWindow = null;
        var show = null;

        var coordFilter = function(args){
            return $filter('latlongContentFilter')(args);
        };

        //initial add coordinates
        var makeMarkers = function(){
            data.forEach(function(row){
                var i = coordFilter(row)
                //ignore test clinic and any others without a DDS assigned to their group
                if (i.dds == null){
                    return;
                }
                //var content = getContent(i);
                var marker = new google.maps.Marker({
                    position: i,
                    map: map,
                    label: i.label,
                    longlabel: i.longlabel,
                    clinicID: i.clinicID,
                    labelOrigin: new google.maps.Point(26.5, 20)
                });
                markers[i.clinicID] = [marker, i];
                marker_array.push(i);
            })
        };

        var addMarkerListeners = function(show_function){
            for(var clinicID in markers){
                var marker = markers[clinicID][0];
                var i = markers[clinicID][1];
                google.maps.event.addListener(marker,'click', (function(marker,i){
                    return function() {
                        show_function(marker);
                    };
                })(marker,i));
            }
        }

        var colorMarkers = function(colorSpecial){
            for(var clinicID in markers){
                //Basic color while waiting for advanced
                //Advanced coloring from child map service
                if (colorSpecial != null) {
                    colorSpecial(markers[clinicID]);
                }else{
                    colorMarker(clinicID, 5)
                }

            }
        }

        //color map on scale of 0-5
        var colorMarker = function(clinicID, level){
            var marker = markers[clinicID][0];
            //$log.info('coloring marker for ', clinicID);
            $rootScope.$broadcast("label-update-" + clinicID, {
                "clinicID":clinicID,
                "iconUrl":getIconUrl(level),
                "level":level
            });
            scope.$on("label-update-request-" + clinicID, function(evt, data){
                //$log.info('request for clinicid received', clinicID);
                $rootScope.$broadcast("label-update-" + clinicID, {
                    "clinicID":clinicID,
                    "iconUrl":getIconUrl(level),
                    "level":level
                });
            });
            marker.setIcon({
                url: getIconUrl(level),
                labelOrigin: new google.maps.Point(15.5, 10),
            });
        }

        //icons for icon intensity
        var getIconUrl = function(level){
            var p = "/static/mapicons/"
            var urls = {
                0: p+"firedept.png",
                1: p+"red.png",
                2: p+"orange.png",
                3: p+"yellow.png",
                4: p+"green.png",
                5: p+"grey.png",
            }
            if (level === true){
                return urls[4];
            }
            if(level === false){
                return urls[1];
            }
            if (urls[level] != null){
                return urls[level];
            }else{
                return urls[5];
            }

        }

        // centers map on clinic, shows data from child _show function
        var center = function(clinicID){
            try{
                var marker = markers[clinicID][0];
                map.setZoom(10);
                map.setCenter(marker.getPosition());
                show(marker);
            } catch (err){
                $log.info("Couldn't find " + clinicID);
                $window.alert("Clinic Location unknown...");
            }
        }


        /*
            Register Listeners from clinic-selector widget
        */
        var scope = $rootScope.$new();
        scope.$on("clinic-list-select", function(event, info){
            center(info.clinicID);
        });

        /*
            Init Map with callbacks
        */

        this.init = function(elem, show_func, color_func, legend_func){
            show = show_func;
            $log.info("MapService caught init");
            CouchService.load('/api/form/python-group/clinic-location/json?reduce=true&group_level=7&stale=update_after').then( function(retData){
                data = retData;
                var initIndex = (data.length - data.length%2)/2;
                var initPos = coordFilter(data[initIndex]);
                $log.info("loaded jsonMap data", initPos);
                map = new google.maps.Map(elem, {
                    center: {lat: initPos.lat, lng: initPos.lng},
                    zoom: 7
                });
                infoWindow = new google.maps.InfoWindow();
                $log.info("rendered map");
                makeMarkers();
                addMarkerListeners(show);
                if (color_func != null){
                    colorMarkers(color_func);
                }
                $log.info('added coordinates');
                if (legend_func != null){
                    $rootScope.$broadcast('set-map-legend', legend_func());
                }
                $rootScope.$broadcast('map-loaded', null);

            });
            return;
        }

        /*
            Exposed to child (map service)
        */

        this.getInfoWindow = function(){
            return infoWindow;
        }
        this.getMap = function(){
            return map;
        }

        this.getMarkers = function(){
            return markers;
        }

        this.getMarkerArray = function(){
            return marker_array;
        }

        this.colorMarker = colorMarker;
        this.getIconUrl = getIconUrl;


    })
    .directive('mapLegend', function($log, $rootScope) {
            return {
                templateUrl: '/static/app/clinicmap/map-legend-2.html',
                replace: true,
                restrict: 'E',
                scope: {
                    name: '@name'
                },
                link: {
                    pre: function(scope, elems, attrs){

                        $log.info('initial chart-legend scope: ', scope.rows);
                        scope.$on('set-map-legend', function(evt, data){
                            $log.info('legend caught event!' , data);
                            scope.data = data;
                        });

                    }
                }
            };
    });

    /*

        Example Controller

    .controller('MapController', function($scope, $compile, $filter, $log, MapService){
        $log.info("start MapController");
        $scope.init = function(elem){
            $log.info('CtrlInit');
            MapService.init(elem, null, null);
            return;
        }
    });
    */

