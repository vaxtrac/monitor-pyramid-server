function createCookie(name,value,days, domain) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; domain=; path=/";
    if (domain != null){
        document.cookie = name+"="+value+expires+"; domain=" + domain +"; path=/";
    }
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}

function langCookieSame() {
    var current = readCookie('googtrans')
    var defined = readCookie('lang')
    if ( current != defined){
        console.info('language mismatch', current, defined);
        return false;
    }
    return true;
}

function setGoogleTranslate(use){
    eraseCookie("useGoogleTranslate");
    createCookie('useGoogleTranslate', use, 365, 'monitor.vaxtrac.com');
}

function useGoogleTranslate(){
    return readCookie("useGoogleTranslate");
}

function setLangCookie(lang){
    console.info(document.cookie);
    console.info('setting language to ' + lang);
    eraseCookie('googtrans');
    eraseCookie('googtrans');
    console.info(document.cookie);
    createCookie('googtrans', lang, 365, 'monitor.vaxtrac.com');
    console.info(document.cookie);
    console.info(readCookie('googtrans'));
}
