# Translations template for monitor.
# Copyright (C) 2016 ORGANIZATION
# This file is distributed under the same license as the monitor project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: monitor 0.0\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2016-07-26 19:19+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.3.4\n"

#: monitor/templates_dev/403.mako:13
msgid "You must be logged into view this page."
msgstr ""

#: monitor/templates_dev/404.mako:7
msgid "404: Page not found."
msgstr ""

#: monitor/templates_dev/base.mako:37
msgid "VaxTrac Monitor"
msgstr ""

#: monitor/templates_dev/base.mako:42
msgid "Homepage"
msgstr ""

#: monitor/templates_dev/base.mako:46 monitor/templates_dev/ticker.mako:43
#: monitor/templates_dev/ticker.mako:69
msgid "Logout"
msgstr ""

#: monitor/templates_dev/base.mako:64
msgid "&copy; VaxTrac. All rights reserved."
msgstr ""

#: monitor/templates_dev/base.mako:66
msgid "click for open source and graphic acknowledgements"
msgstr ""

#: monitor/templates_dev/base.mako:70
msgid "we use foundation css"
msgstr ""

#: monitor/templates_dev/base.mako:71
msgid "some art designed by freepik"
msgstr ""

#: monitor/templates_dev/clinic_browse.mako:7
msgid "Select Clinic"
msgstr ""

#: monitor/templates_dev/clinic_browse.mako:15
#: monitor/templates_dev/clinic_status.mako:18
msgid "Search / Registration Frequency"
msgstr ""

#: monitor/templates_dev/clinic_browse.mako:54
#: monitor/templates_dev/clinic_status.mako:59
msgid "Searches by Type"
msgstr ""

#: monitor/templates_dev/clinic_map.mako:5
msgid "Simple Map"
msgstr ""

#: monitor/templates_dev/clinic_status.mako:77
msgid "Completions Vs. Outstanding"
msgstr ""

#: monitor/templates_dev/login.mako:5
msgid "Login with Google"
msgstr ""

#: monitor/templates_dev/me-adherence-select.mako:4
msgid "Select Vaccine for Adherence View"
msgstr ""

#: monitor/templates_dev/me-adherence.mako:4
msgid "Dosing/ Schedule Adherence View"
msgstr ""

#: monitor/templates_dev/me-adherence.mako:15
msgid "Dose Provided at Weeks of Age."
msgstr ""

#: monitor/templates_dev/me-adherence.mako:26
#: monitor/templates_dev/me-form-type.mako:27
#: monitor/templates_dev/me-form-type.mako:36
#: monitor/templates_dev/me-latency.mako:21
#: monitor/templates_dev/me-latency.mako:30
msgid "Up a Level."
msgstr ""

#: monitor/templates_dev/me-form-type.mako:4
msgid "Form Type View"
msgstr ""

#: monitor/templates_dev/me-form-type.mako:15
msgid "Search"
msgstr ""

#: monitor/templates_dev/me-form-type.mako:17
msgid "Registraion"
msgstr ""

#: monitor/templates_dev/me-form-type.mako:20
msgid "Search Types"
msgstr ""

#: monitor/templates_dev/me-latency.mako:4
msgid "Latency View"
msgstr ""

#: monitor/templates_dev/ticker.mako:28 monitor/templates_dev/ticker.mako:54
msgid "Navigation Menu"
msgstr ""

#: monitor/templates_dev/ticker.mako:30 monitor/templates_dev/ticker.mako:56
msgid "Project Status"
msgstr ""

#: monitor/templates_dev/ticker.mako:31 monitor/templates_dev/ticker.mako:57
msgid "Clinic Status Map"
msgstr ""

#: monitor/templates_dev/ticker.mako:32 monitor/templates_dev/ticker.mako:58
msgid "Browse Clinics"
msgstr ""

#: monitor/templates_dev/ticker.mako:35 monitor/templates_dev/ticker.mako:61
msgid "Monitor & Evaluation"
msgstr ""

#: monitor/templates_dev/ticker.mako:36 monitor/templates_dev/ticker.mako:62
msgid "Schedule Adherence"
msgstr ""

#: monitor/templates_dev/ticker.mako:37 monitor/templates_dev/ticker.mako:63
msgid "Types of Forms Submitted"
msgstr ""

#: monitor/templates_dev/ticker.mako:38 monitor/templates_dev/ticker.mako:64
msgid "Data Latency"
msgstr ""

#: monitor/templates_dev/ticker.mako:41 monitor/templates_dev/ticker.mako:67
msgid "Administration"
msgstr ""

#: monitor/templates_dev/ticker.mako:42 monitor/templates_dev/ticker.mako:68
msgid "Usage History"
msgstr ""

#: monitor/templates_dev/ticker.mako:81
msgid "Recent Submissions"
msgstr ""

#: monitor/templates_dev/usage_total.mako:4
#: monitor/templates_dev/usage_user.mako:4
msgid "User Activity"
msgstr ""

#: monitor/templates_dev/usage_total.mako:13
msgid "User"
msgstr ""

#: monitor/templates_dev/usage_total.mako:14
msgid "Last Visit"
msgstr ""

#: monitor/templates_dev/usage_total.mako:15
msgid "Last Page"
msgstr ""

#: monitor/templates_dev/usage_total.mako:16
msgid "Page Hits"
msgstr ""

#: monitor/templates_dev/usage_user.mako:13
msgid "Newer 20"
msgstr ""

#: monitor/templates_dev/usage_user.mako:18
msgid "Older 20"
msgstr ""

