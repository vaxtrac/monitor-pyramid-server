import json
import couchdb
from beaker.cache import cache_region
import urllib2
from urllib import urlencode
from monitor.settings import Settings

SETTINGS = Settings

_couch_user = SETTINGS['couchdb_user']
_couch_pw = SETTINGS['couchdb_password']
_couch_url = "localhost:5984"
_couch_server_string =  "http://%s:%s@%s" % (_couch_user, _couch_pw, _couch_url)

USAGEDB = 'usage'

class WriteableCouch(object):

    server = couchdb.Server(_couch_server_string)

    def __init__(self):
        self.Server = WriteableCouch.server

    def getDataBase(self, db):
        try:
            return self.Server[db]
        except Exception,e:
            print e
            return None

    def query(self, db, design, view,**args):
        try:
            database = self.getDataBase(db)
            result = database.view("%s/%s" % (design,view), **args)
            return result
        except Exception,e:
            print "Error in writeable couch query, %s" % ((db,design,view,args,),)
            print e
            return

    @cache_region('long_term')
    def cachedQuery(self, db, design, view, **args):
        return self.filterQuery(db, design, view, **args)

    def filterQuery(self, db, design, view,**args):
        startkey = args.get("startkey")
        endkey = args.get("endkey")
        if (not (startkey and endkey)):
            if args:
                q = self.query(db, design, view, **args)
            else:
                q = self.query(db, design, view)
            if args.get("exclude_null", False) == True:
                rows = [row for row in q if row.get("value")]
            else:
                rows = [i for i in q]
            return json.dumps({"rows":rows})
        q = self.query(db, design, view,**args)
        key_size = min([len(l) for l in [startkey,endkey]])
        divergence = [ startkey[idx] == endkey[idx] for idx in range(key_size)]
        start = next(i for i,x in enumerate(divergence) if x == False)
        rows = [i for i in q]
        #print len(rows)
        for x, diverge in enumerate(divergence):
            if not diverge and startkey[x] < endkey[x]:
                rows = [row for row in rows if row.get("key")[x] >= startkey[x] and row.get("key")[x] <= endkey[x]]
            elif startkey[x] > endkey[x]:
                rows = [row for row in rows if row.get("key")[x] >= startkey[x] or row.get("key")[x] <= endkey[x]]
            elif diverge:
                rows = [row for row in rows if row.get("key")[x] == startkey[x]]
            #print "%s | %s | %s" % (x, diverge, len(rows))
        #print "exported rows" % (len(rows))
        if args.get("exclude_null") == True:
            rows = [row for row in rows if row.get("value")]
            #print "after exclusions: " % (len(rows))
        return json.dumps({"rows":rows})


class CouchDBAPI(object):

    def __init__(self):
        self.user = _couch_user
        self.pw = _couch_pw
        self.base = _couch_url
        self.serverString = "http://%s"
        self._conn = self.serverString % (self.base)
        self.connect()

    def connect(self):
        password_mgr = urllib2.HTTPPasswordMgrWithDefaultRealm()
        password_mgr.add_password(None, 'http://%s' % self.base, self.user, self.pw)
        handler = urllib2.HTTPBasicAuthHandler(password_mgr)
        self.opener = urllib2.build_opener(handler)

    def _getView(self, db, design, view, **args):
        url = 'http://%s/%s/_design/%s/_view/%s' % (self.base, db, design, view)
        if not args:
            response = self.opener.open(url)
        else:
            data = urlencode(args)
            response = self.opener.open("%s?%s" % (url, data))
        return response

    def view(self, db, design, view, **args):
        print "API request to %s -> %s/%s ? %s" % (db, design, view, args,)
        try:
            resp = self._getView(db, design, view, **args)
            return resp.read()
        except Exception ,e:
            print "API ERROR %s" % (e)
            if '401' in str(e):
                try:
                    print "attempting reconnect to DB"
                    self.connect()
                    resp = self._getView(db,design,view,**args)
                    return resp.read()
                except Exception, e2:
                    print "could not recover from 401, Error: %s" % (e2)
            return {"error":"bad_request","reason":"invalid_json"}

    def changes(self, db, **args):
        url = 'http://%s/%s/_changes' % (self.base, db)
        print "Changes API request to url %s %s" % (url, args,)
        try:
            if not args:
                response = self.opener.open(url)
            else:
                data = urlencode(args)
                response = self.opener.open("%s?%s" % (url, data))
            return response.read()
        except Exception ,e:
            print "API ERROR in _changes %s" % (e)
            return {"error":"bad_request","reason":"invalid_json"}
WCouch = WriteableCouch()
CouchAPI = CouchDBAPI()

if __name__ == "__main__":
    args = {"descending":"false", "limit":1}
    print args
    print CouchAPI.changes("form", **args)
    args["descending"] = "true"
    args["include_docs"] = "true"
    print args
    print CouchAPI.changes("form", **args)
    '''
    args = {"startkey":["OPV","0","2015","10","AZT"], "endkey":["OPV","0","2016","01","AZT",{}],"reduce":True, "group_level":6}
    x = WCouch.filterQuery("case", "py-adherence","drill-view", **args)
    print x
    '''
    pass


